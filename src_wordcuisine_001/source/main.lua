--[[

version 0.0.1
[x] Added dish progress "states"
[x] Removed spoon from main game
[x] Fixed button sizes (quit, resume, facebook, remove ads, etc.)
[x] Removed letter tints on plate
[x] Changed mat, table and cloth order
[x] Replaced dish titles with cuisine
[x] Changed version number setup
[x] Added admob IDs for Word Odyssey
[x] Added Facebook App ID for Word Odyssey
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.2
- stretched menu screen to fill to the bottom
- different content layout for iphone X screens
- implemented new title screen
- center anchored the title screen
- adjusted puzzle name a bit to the right
- implemented new "Extra Words" button
- fixed invalid file names and invalid files
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.3
- extra words dialogue box animation (locked and unlocked)
- implemented all food assets
- fixed math on puzzle state
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.4
- extra words button effect like the coin thing
- claim button disappears if not claimable
- new app icon
- new iphone title screen
- removed letter coloring
-  "more dishes coming soon!" with new doggo animation
- adjusted button sizes and font sizes
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.5
- fixed dish and letters placement (anchored to top rather than on the center)
- font - options
- font - shop
- font and coloring - menu and level select
- font - game and end screen
- end level graphic
- font - credits
- game goes to the next level after finishing instead of going to the level select screen
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.6
- new popup words
- fix sale icon emphasis bug
- hopefully fixed then next level bug
- fix extra words button bug
- ipad adjustments
- new how to play image

*disabled pause and shop buttons on
	- opening extra words
	- end level screen
	- pause screen
- bug fix on touch release event

- puzzle shuffling
-hopefully fixed this: a bug where you spam the next level button and it still works even though it disappears
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.7
- fix ipad aspect ratio on other phones
- center the pause and results popups
- touch release on table and word box bg
- removed results screen hamster
- shop works now
- readjusted buttons size to match game's
- ad banner removed when on main menu
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.8
- fixed a bug wherein the extra words button doesn't resume shaking when quitting then reentering a level
- interstitial ads every 3 levels but only after Chilli con Carne
- fixed android toolbar appearing after opening shop
- build number properly anchored to bottom left of screen 
- locked vertical scroll of shop

* facebook group popup 
	- opens fb group page when clicked then never reappears again
	- firebase analytics when clicked
	- if closed, opens again after 2 days


* rate up popup
	- layout of dialog and text and buttons
	- shows only when finishing a dish
	- if no, returns again on the next dish finish
	- if yes, makes a rate us popup [native.showPopup( "rateApp", options )] - opens the playstore and won't appear again
	
- delay on fb popup
- placement of win dialog
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 0.0.9
- new fb popup graphics
- new watch ad for free hint popup graphics
- ios builds

]]--

--al = nil; if audio2 then audio=audio2; print("=====>>   Using audio2   <<=====") end

local launchArgs = ...

admob = require( "plugin.admob" )
whichPlatform = system.getInfo( "platformName" )    --> Used to check for platforms
gpgs = nil
gameCenter = nil

local composer = require "composer"
local facebook = require "plugin.facebook.v4a"
local notifications = require( "plugin.notifications.v2" )

firebaseAnalytics = require "plugin.firebaseAnalytics"
widget = require( "widget" ) --> Needed to support retina text

require "settings"
json = require( "json" )

targetAppStore = system.getInfo( "targetAppStore" )
if ( "apple" == targetAppStore ) then  -- iOS
    store = require( "store" )
elseif ( "google" == targetAppStore ) then  -- Android
    store = require( "plugin.google.iap.v3" )
elseif ( "amazon" == targetAppStore ) then  -- Amazon
    store = require( "plugin.amazon.iap" )
else
    print( "In-app purchases are not available for this platform." )
end

--system.deactivate ("multitouch")

local function facebookListener( event )
    if ( "fbinit" == event.name ) then
    	print( "Facebook initialized" )
    	facebook.publishInstall()
    elseif ( "fbconnect" == event.name ) then
        if ( "session" == event.type ) then
            -- Handle login event and try to share the link again if needed
        elseif ( "dialog" == event.type ) then
            -- Handle dialog event
        end
    end
end

facebook.init( facebookListener ) --> Init Facebook
firebaseAnalytics.init() --> Init Firebase 

-- Set Defaults Here
display.setDefault( "isAnchorClamped", false )
system.activate( "multitouch" )

gameTitle = "Word Cuisine"
buildNumber = "0.0.9"
lastScene = ""
--keystore password : p0psicleg@me$
loggedIntoGC = false
isOnline = false

-- used to move the back buttons lower as to not be hidden by the speaker of the Iphone X
onIphoneX = ( string.find( system.getInfo("architectureInfo"), "iPhone10,3" ) ~= nil ) or
                  ( string.find( system.getInfo("architectureInfo"), "iPhone10,6" ) ~= nil )

--[[ code above doesnt work on simulator so set it to true for now.
	DONT FORGET TO COMMENT CODE BELOW WHEN READY TO BUILD --]]
--onIphoneX = ( string.find( system.getInfo("platform"), "ios" ) ~= nil )

--> Init Google Play Services
-- if ( not amazonBuild ) then
	-- print("NOT AMAZON")
	
	-- if ( whichPlatform == "Android") then
		-- gpgs = require( "plugin.gpgs" )
	-- elseif ( whichPlatform == "iPhone OS") then
		-- gameCenter = require( "gameNetwork" )
	-- end
	
-- end

function testNetworkConnection()
    local socket = require("socket")
    local test = socket.tcp()
    test:settimeout( 1000 )  -- Set timeout to 1 second
    local netConn = test:connect( "www.google.com", 80 )
    if ( netConn == nil ) then
        return false
    end
    test:close()
    return true
end

-- Google Play Games Services initialization/login listener
local function gpgsInitListener( event )
	print(">>>>>>>>>>>>>>>>>>> INIT INIT'D")
    if not event.isError then
		print(">>>>>>>>>>>>>>>>>>> NOT ERROR EVENT NAME IS " .. event.name)
        if ( event.name == "init" ) then  -- Initialization event
            -- Attempt to log in the user
			print(">>>>>>>>>>>>>>>>>>> INIT ")
            gpgs.login( { userInitiated=true, listener=gpgsInitListener } )
        elseif ( event.name == "login" ) then  -- Successful login event
            print( json.prettify(event) )
			print("Has Logged In")
			loggedIntoGC = true
        end
	else
		print("ERROR: " .. event.errorMessage .. " " .. event.errorCode)
    end
end

-- Apple Game Center initialization/login listener
local function gcInitListener( event )
    if event.data then  -- Successful login event
        print( json.prettify(event) )
    end
end

-- if gpgs then
	-- Initialize game network based on platform
	-- if isOnline then
		-- Initialize Google Play Games Services
		-- gpgs.init( gpgsInitListener )
		-- print(">>>>>>>>>>>>>>>>>>> GPGS")
	-- elseif ( whichPlatform == "iPhone OS" and isOnline) then
		-- Initialize Apple Game Center
		-- gameCenter.init( "gamecenter", gcInitListener )
		-- print("GC")
	-- end
-- end

--> ADMOB Initialization
function admobListener( event )
    if ( event.phase == "init" ) then  -- Successful initialization
        print( event.provider )
        print( "PRELOADING ADS..." )
        if ( whichPlatform == "iPhone OS" ) then
            admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/2539968517" } )
			admob.load( "banner", { adUnitId = "ca-app-pub-8768005339469302/3201193931" } )
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/7189659961" } )
        elseif ( whichPlatform == "Android" ) then
            admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/8119721966" } )
			admob.load( "banner", { adUnitId = "ca-app-pub-8768005339469302/9631990475" } )
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/3466161085" } )
        end
    elseif ( event.phase == "reward" ) then --> Video ad successfully completed
		if ( whichPlatform == "iPhone OS" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/7189659961" } )
        elseif ( whichPlatform == "Android" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/3466161085" } )
        end
		rewardAdBool = false
		rewardVideoComplete = true
		settings.set('rewardAdBool', rewardAdBool)
    elseif ( event.phase == "closed" ) then --> Interstitial ad successful and closed
        print( "Ad closed!" )
        print( "RELOADING ADS..." )  --> Refresh banner ad
		if (whichPlatform == "iPhone OS" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/7189659961" } )
       		admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/2539968517" } )
        elseif (whichPlatform == "Android" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/3466161085" } )
        	admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/8119721966" } )
        end
		
    end
end

--> Init AdMob, replace these with app-specific AdMob IDs
if ( whichPlatform == "iPhone OS" ) then
    adappID = "ca-app-pub-8768005339469302~7935765425"
elseif ( whichPlatform == "Android" ) then
    adappID = "ca-app-pub-8768005339469302~9411379061"
end

admob.init( admobListener, { appId = adappID, testMode = false } )


--> in game default values
extraWordsVal = 0
tempExVal = 0
bannerHeight = 0
adCounter = 0
coinVal = 0
levelNum = 1
doRateUs = true
rateDone = false

rewardAdBool = false
saleShopBool = false
checkForSale = false
dateOfSale = nil
shopOpen = false

showFBGroupPopup = true

audios = {}
mydata = nil
dataLevels = {}
validProducts = {}
invalidProducts = {}

audios.sfx = true
audios.music = true
ads = true
productsLoaded = false
storeLoaded = false

local levelOptions = {levelNum = 1, locked = false, complete = false, soupName = "Tomato Soup", levelName = "Onion"}

T_achievements = 
{
	{achievementID = "CgkI2JGyxtoCEAIQAQ", treatName = "Chili con Carne", lastIng = "Pepper", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQAg", treatName = "Cream of Mushroom Soup", lastIng = "Mushroom", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQAw", treatName = "Spaghetti with Meatballs", lastIng = "Pasta", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBA", treatName = "Xiao Long Bao", lastIng = "Soy Sauce", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBQ", treatName = "Bacon and Cheese Quiche", lastIng = "Bacon", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBg", treatName = "Taco Salad", lastIng = "Beef", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBw", treatName = "Shepherd's Pie", lastIng = "Lamb", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQCA", treatName = "Gyudon", lastIng = "Beef", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQCQ", treatName = "Chicken Curry", lastIng = "Chicken", complete = false, awarded = false}
}

function loadData()
	if ( mydata == nil ) then 
		mydata = {} 
		initializeData()
		for i = 1, #mydata do
			if settings.get('mydata[' .. i .. ']') ~= nil then
				local tempdata = {}
				tempdata[i] = settings.get('mydata[' .. i .. ']')
				mydata[i].complete = tempdata[i].complete
				mydata[i].locked = tempdata[i].locked
				print ("LOADING LEVEL DATA: " .. i)
			end
		end
	end
	
	for i = 1, #T_achievements do
		if settings.get('achievements[' .. i .. ']') ~= nil then
			local tempdata = {}
			tempdata[i] = settings.get('achievements[' .. i .. ']')
			T_achievements[i] = tempdata[i]
			print ("LOADING ACHIEVEMENTS DATA: " .. i)
		end
	end
	
	dateOfSale = settings.get('dateOfSale')
	if ( dateOfSale == nil) then 
		dateOfSale = {}	
	end
	
	coinVal = settings.get('coins')
	if(coinVal == 0 or coinVal == nil) then coinVal = 0 end
	
	saleShopBool = settings.get('saleShopBool')
	if ( saleShopBool == nil) then
		saleShopBool = false
	end
	
	checkForSale = settings.get('checkForSale')
	if checkForSale == nil then
		checkForSale = false
	end
	
	if checkForSale == false then
		saleShopBool = false
	end
	
	rewardAdBool = settings.get('rewardAdBool')
	if ( rewardAdBool == nil) then rewardAdBool = false end
	
	dataLevels = settings.get('dataLevels')
	if ( dataLevels == nil) then dataLevels = {} table.insert(dataLevels,levelOptions) end

	extraWordsVal = settings.get('extraWordsVal')
	if ( extraWordsVal  == nil or extraWordsVal == 0) then extraWordsVal = 0 end
	
	tempExVal = settings.get('tempExVal')
	if ( tempExVal  == nil or tempExVal == 0) then tempExVal = 0 end
	
	audios.sfx = settings.get('sfx')
	if(audios.sfx == nil) then audios.sfx = true end
	
	audios.music = settings.get('music')
	if(audios.music == nil) then audios.music = true end
	
	showFBGroupPopup = settings.get('fb group')
	if(showFBGroupPopup == nil) then showFBGroupPopup = true end

	ads = settings.get('ads')
	if(ads == nil) then ads = true end
	
	validProducts = settings.get('validProducts')
	if validProducts == nil then validProducts = nil end
	
	doRateUs = settings.get('doRateUs')
	if ( doRateUs == nil) then doRateUs = false end
	
	rateDone = settings.get('rateDone')
	if ( rateDone == nil) then rateDone = false end

	print("SAVED DATA LOADED SUCCESSFULLY")
end


-- for puzzleData 
local function createData(typeName, productName, soupName, whichPuzzleSet, label, soupIng, locked, complete, lettercount, levelcount) 
	local valType = 0
	local rawData = {}
	if typeName == "header" then
		valType = 1
		rawData = {type = valType}
	elseif typeName == "product" then
		valType = 2
		rawData = {type = valType, label = productName, locked = locked, soupName = soupName}
	elseif typeName == "ingredient" then
		valType = 3
		rawData = {type = valType, label = label, whichPuzzleSet = whichPuzzleSet, locked = locked, 
					complete = complete, lettercount = lettercount, levelcount = levelcount, productName = productName, soupName = soupName, soupIng = soupIng}
	elseif typeName == "footer" then
		valType = 4
		rawData = {type = valType}
	elseif typeName == "other" then
		valType = 5
		rawData = {type = valType}
	end
	table.insert(mydata, rawData)
end

function initializeData ()
	-- typeName, productName, soupName, whichPuzzleSet, label, soupIng, locked, complete, lettercount, levelcount
	local isLocked = 1
	createData("header")
	createData("product", "Chili con Carne", "Tomato Soup", nil, nil, nil, 0)
	createData("ingredient", "Chili con Carne", "Tomato Soup", 1, "Beef", "Onion", 0, 0, 3, 5)
	createData("ingredient", "Chili con Carne", "Tomato Soup", 2, "Pepper", "Tomato", isLocked, 0, 3, 10)
	createData("footer")
		
	createData("header")
	createData("product", "Cream of Mushroom Soup", "Pumpkin Soup", nil, nil, nil, 1)
	createData("ingredient", "Cream of Mushroom Soup", "Pumpkin Soup", 3, "Onion", "Leek", isLocked, 0, 4, 20)
	createData("ingredient", "Cream of Mushroom Soup", "Pumpkin Soup", 4, "Chicken Broth", "Garlic", isLocked, 0, 4, 20)
	createData("ingredient", "Cream of Mushroom Soup", "Pumpkin Soup", 5, "Butter", "Cream", isLocked, 0, 4, 20)
	createData("ingredient", "Cream of Mushroom Soup", "Pumpkin Soup", 6, "Cream", "Black Pepper", isLocked, 0, 5, 20)
	createData("ingredient", "Cream of Mushroom Soup", "Pumpkin Soup", 7, "Mushroom", "Pumpkin", isLocked, 0, 5, 20)
	createData("footer")	

	createData("header")
	createData("product", "Spaghetti with Meatballs", "Chicken Noodle Soup", nil, nil, nil, 1)
	createData("ingredient", "Spaghetti with Meatballs", "Chicken Noodle Soup", 8,  "Tomato", "Celery", isLocked, 0, 5, 20)
	createData("ingredient", "Spaghetti with Meatballs", "Chicken Noodle Soup", 9,  "Garlic", "Oregano", isLocked, 0, 5, 20)
	createData("ingredient", "Spaghetti with Meatballs", "Chicken Noodle Soup", 10, "Parmesan Cheese", "Basil", isLocked, 0, 5, 20)
	createData("ingredient", "Spaghetti with Meatballs", "Chicken Noodle Soup", 11, "Beef", "Egg Noodles", isLocked, 0, 5, 20)
	createData("ingredient", "Spaghetti with Meatballs", "Chicken Noodle Soup", 12, "Pasta", "Chicken Broth", isLocked, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Xiao Long Bao", "Egg Drop Soup", nil, nil, nil, 1)
	createData("ingredient", "Xiao Long Bao", "Egg Drop Soup", 13, "Pork", "Egg", isLocked, 0, 5, 20)
	createData("ingredient", "Xiao Long Bao", "Egg Drop Soup", 14, "Flour", "Cornstarch", isLocked, 0, 5, 20)
	createData("ingredient", "Xiao Long Bao", "Egg Drop Soup", 15, "Ginger", "Green Onion", isLocked, 0, 5, 20)
	createData("ingredient", "Xiao Long Bao", "Egg Drop Soup", 16, "Wine", "Chicken Broth", isLocked, 0, 5, 20)
	createData("ingredient", "Xiao Long Bao", "Egg Drop Soup", 17, "Soy Sauce", "Soy Sauce", isLocked, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Bacon and Cheese Quiche", "Cream of Mushroom", nil, nil, nil, 1)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 18, "Egg", "Fresh Mushroom", isLocked, 0, 5, 20)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 19, "Cream", "Onion", isLocked, 0, 5, 20)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 20, "Parmesan Cheese", "Garlic", isLocked, 0, 5, 20)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 21, "Swiss Cheese", "Butter", isLocked, 0, 5, 20)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 22, "Onion", "Evaporated Milk", isLocked, 0, 6, 20)
	createData("ingredient", "Bacon and Cheese Quiche", "Cream of Mushroom", 23, "Bacon", "Nutmeg", isLocked, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Taco Salad", "Clam Chowder", nil, nil, nil, 1)
	createData("ingredient", "Taco Salad", "Clam Chowder", 24, "Tomato", "Cream", isLocked, 0, 6, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 25, "Lettuce", "Minced Clams", isLocked, 0, 6, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 26, "Red Wine Vinegar", "Onion", isLocked, 0, 7, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 27, "Tortilla Chips", "Celery", isLocked, 0, 7, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 28, "Onion", "Potato", isLocked, 0, 7, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 29, "Cheddar Cheese", "Carrot", isLocked, 0, 7, 20)
	createData("ingredient", "Taco Salad", "Clam Chowder", 30, "Beef", "Butter", isLocked, 0, 7, 20)
	createData("footer")

	createData("header")
	createData("product", "Shepherd's Pie", "Tortilla Soup", nil, nil, nil, 1)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 31, "Butter", "Tortilla", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 32, "Potato", "Jalapeño", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 33, "Flour", "Cilantro", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 34, "Rosemary", "Bell Pepper", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 35, "Thyme", "Ground Beef", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 36, "Garlic", "Cheddar Cheese", isLocked, 0, 7, 20)
	createData("ingredient", "Shepherd's Pie", "Tortilla Soup", 37, "Lamb", "Chipotle Chili", isLocked, 0, 7, 20)
	createData("footer")
	
	createData("header")
	createData("product", "Gyudon", "Blueberry Pie", nil, nil, nil, 1)
	createData("ingredient", "Gyudon", "Blueberry Pie", 38, "Green Onion", "Flour", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 39, "Sake", "Shortening", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 40, "Soy Sauce", "Butter", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 41, "Rice", "Lemon Juice", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 42, "Red Ginger", "Cornstarch", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 43, "Egg", "Cinnamon", isLocked, 0, 7, 20)
	createData("ingredient", "Gyudon", "Blueberry Pie", 44, "Beef", "Blueberries", isLocked, 0, 7, 20)
	createData("footer")
	
	createData("header")
	createData("product", "Chicken Curry", "Carrot Cake", nil, nil, nil, 1)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 45, "Coconut Milk", "Icing Sugar", isLocked, 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 46, "Ginger", "Vanilla", isLocked, 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 47, "Garlic", "Nutmeg", isLocked, 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 48, "Tomato Sauce", "Raisins", isLocked, 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 49, "Onion", "Walnuts", isLocked , 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 50, "Curry Powder", "Cream Cheese", isLocked, 0, 7, 20)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 51, "Chicken", "Carrots", isLocked, 0, 7, 20)
	createData("footer")
	
	--[[
	createData("header")
	createData("product", "Chicken Curry", "Carrot Cake", nil, nil, nil, 1)
	createData("ingredient", "Chicken Curry", "Carrot Cake", 52, "Test2", "Test3", 0, 0, 4, 1)
	createData("footer")--]]
	
	createData("other")	
end

--Composer Transition
composer.effectList.slideOverRight = {sceneAbove = false,
      concurrent = true ,
      from =  {  yStart = 0 , xStart = 0 , xEnd = display.contentWidth+100, transition = easing.inQuad , yEnd = 0},
      to = {yStart = 0, xStart = 0 , xEnd = 0, alphaStart = 0.1, alphaEnd = 1, transition = easing.inQuad, yEnd = 0 },
    }

composer.effectList.slideOverLeft = {sceneAbove = false,
      concurrent = true ,
      from =  {  yStart = 0 , xStart = 0 , xEnd = -display.contentWidth-100, transition = easing.inQuad , yEnd = 0},
      to = {yStart = 0, xStart = 0 , xEnd = 0, alphaStart = 0.1, alphaEnd = 1, transition = easing.inQuad, yEnd = 0 },
    }
	
--> Used for anchoring objects
anchor = {
    TopLeft = function(t) t.anchorX, t.anchorY = 0, 0; end,
    TopCenter = function(t) t.anchorX, t.anchorY = .5, 0; end,
    TopRight = function(t) t.anchorX, t.anchorY = 1, 0; end,
    CenterLeft = function(t) t.anchorX, t.anchorY = 0, .5; end,
    Center = function(t) t.anchorX, t.anchorY = .5, .5; end,
    CenterRight = function(t) t.anchorX, t.anchorY = 1, .5; end,
    BottomLeft = function(t) t.anchorX, t.anchorY = 0, 1; end,
    BottomCenter = function(t) t.anchorX, t.anchorY = .5, 1; end,
    BottomRight = function(t) t.anchorX, t.anchorY = 1, 1; end,
}

-- Load Image Ceate Image
function newImageRectNoDimensions( filename , loc)

    if loc == nil then loc = system.ResourceDirectory end
    local w, h = 0, 0
    local image
    image = display.newImage( filename, loc )
    w =  math.floor(image.width)
    h =  math.floor(image.height)
    image:removeSelf()

    return display.newImageRect( filename, loc, w, h )
end

-- audio
audio.reserveChannels( 5 )
bgm = audio.loadStream("res/audio/BGM_GunmaGambol.mp3")

function loadSoundTable()
	soundTable = {
		letterSelect = audio.loadSound( "res/audio/LetterSelect.mp3" ),
		letterShuffle = audio.loadSound("res/audio/LetterShuffle.mp3"),
		showHint = audio.loadSound("res/audio/ShowHint.mp3"),
		--HidePause = audio.loadSound( "res/audio/HidePause.mp3" ), -- not valid
		--showPause = audio.loadSound( "res/audio/ShowPause.mp3" ), -- not valid
		luckyBowl = audio.loadSound("res/audio/ShowLuckyBowl.mp3"),
		claimGold = audio.loadSound( "res/audio/GoldClaim.mp3" ),
		--popUp = audio.loadSound("res/audio/Pop-ups.mp3"), -- not valid
		wordFound = audio.loadSound("res/audio/FoundWord.mp3"),
		extraWord = audio.loadSound("res/audio/ExtraWord.mp3"),
		noWord = audio.loadSound("res/audio/WrongWord.mp3"),
		dupWord = audio.loadSound("res/audio/RepeatWord.mp3"),
		coinBomb = audio.loadSound("res/audio/CoinBomb.mp3"),
		levelEnd = audio.loadSound("res/audio/LevelEnd.mp3"),
		
		-- Temporary replacement for invalid audios
		HidePause = audio.loadSound("res/audio/ShowLuckyBowl.mp3"),
		showPause = audio.loadSound("res/audio/ShowLuckyBowl.mp3"),
		popUp = audio.loadSound("res/audio/ShowLuckyBowl.mp3")
	}	
end
function audioButtonSelect()
	buttonSelect = audio.loadSound("res/audio/ButtonSelect.mp3")
	audio.play(buttonSelect, {channel = 2})
end

if audios.sfx then
	for i = 2,32 do
		audio.setVolume(0.5, {channel=i})
	end	
else
	for i = 2,32 do
		audio.setVolume(0, {channel=i})
	end	
end

audio.play(bgm, { channel=1, loops=-1,  } )
if audios.music then
	audio.setVolume( 1, { channel=1 } )
else
	audio.setVolume( 0, { channel=1 } )
end

--Change a block color
function randomFillColor(obj)
	local R = tonumber("."..math.random(0,9))
	local G = tonumber("."..math.random(0,9))
	local B = tonumber("."..math.random(0,9))
	obj:setFillColor(R,G,B)
end

--Delta Time
local runtime = 0
 
function getDeltaTime()
    local temp = system.getTimer()  -- Get current game time in ms
    local dt = (temp-runtime) / (1000/60)  -- 60 fps or 30 fps as base
    runtime = temp  -- game time
    return dt
end

display.setStatusBar( display.HiddenStatusBar )

--For Android 6, hide bottom bar
if system.getInfo("platformName") == "Android" then
    if system.getInfo("androidApiLevel") >= 23 then
		native.setProperty( "androidSystemUiVisibility", "immersive" )
        native.setProperty( "androidSystemUiVisibility", "immersiveSticky" )
    end
end

function checkMemory()
   collectgarbage( "collect" )
   local memUsage_str = string.format( "MEMORY = %.3f KB", collectgarbage( "count" ) )
   print( memUsage_str)--, "TEXTURE = "..(system.getInfo("textureMemoryUsed") / (1024 * 1024) ) )
end
--timer.performWithDelay( 1000, checkMemory, 0 )  --uncomment to determine memory usage

bannerVisible = false
rewardVideoComplete = false

local function onNotification( event )
	if event.type == "remote" then
	elseif event.type == "local" then
		native.setProperty( "applicationIconBadgeNumber", 0 ) -- resets notification badge when app is opened from notif
	end
end

local dateToday
local pattern = "(%d+)%-(%d+)%-(%d+)%/(%d+):(%d+):(%d+)"
local dyear, dmonth, dday, dhour, dmin, dsec 
local xyear, xmonth, xday, xhour, xmin, xsec

cday = 0
chour = 0
cmin = 0
csec = 0


-- Checking for SALE and TIME ELAPSED AFTER SALE INITIATION
function checkTime()
	if checkForSale then
		print("---------------------------------")
		print("CHECKING TIME ....")
		local thisDate = os.date("*t")
		dateToday = tostring(os.date("%Y-%m-%d/%H:%M:%S", os.time(t)))
		print("---------------------------------")
		print("DATE TODAY: " .. dateToday)
		dyear, dmonth, dday, dhour, dmin, dsec = dateToday:match(pattern)
	
		if dateOfSale.sT ~= nil then
			print("START SALE TIME: " .. dateOfSale.sT)
			print("---------------------------------")
		
			saleShopBool = false
			
			xyear, xmonth, xday, xhour, xmin, xsec = dateOfSale.sT:match(pattern)
			
			local function compareHour()
				if xhour == dhour then
					if dmin > xmin then
						saleShopBool = true
					elseif dmin == xmin then
						if dsec >= xsec then
							saleShopBool = true
						end
					else
						saleShopBool = false
					end	
				else
					saleShopBool = true
				end
			end
			
			if xyear == dyear then
				if xmonth == dmonth and dday >= xday then				
					if dday == xday and dhour >= xhour then
						saleShopBool = true
						compareHour()
					end
					if (dday+0) == (xday+1) and xhour >= dhour then
						if xhour == dhour then
							if dmin > xmin then
								saleShopBool = false
							else
								saleShopBool = true
							end	
						elseif xhour > dhour then
							saleShopBool = true
						else
							saleShopBool = false
						end
					end
				elseif dmonth > xmonth and dday == "01" then
					if xhour >= dhour then
						if xhour == dhour then
							if xmin > dmin then
								saleShopBool = true
							elseif dmin == xmin then
								if xsec >= dsec then
									saleShopBool = true
								end
							else
								saleShopBool = false
							end	
						elseif xhour > dhour then
							saleShopBool = true
						end
					end
				end
			end
			
			local function compareSec()
				if xsec > dsec then
					csec = 60 - ( xsec - dsec )
					cmin = cmin - 1
				elseif dsec > xsec then
					csec = dsec - xsec
				else
					csec = 0
				end
			end
			
			if saleShopBool then
			-- TIME ELAPSED COMPUTATION [EDIT AT YOUR OWN RISK]
				if dhour == xhour and dday == xday then -- same day
					if dmin > xmin then
						if dsec > xsec then
							cmin = dmin - xmin
							csec = dsec - xsec
						elseif xsec > dsec then
							cmin = dmin - xmin
							cmin = cmin - 1
							
							csec = 60 - ( xsec - dsec )
						else
							csec = 0
						end
					end
				elseif dhour > xhour and dday == xday then -- same day
					chour = dhour - xhour
					if xmin > dmin then
						cmin = xmin - dmin
						compareSec()
					elseif xmin < dmin then
						cmin = dmin - xmin
						compareSec()
					else
						if xsec > dsec then
							csec = xsec - dsec
							cmin = 59
							chour = chour - 1
						elseif xsec < dsec then
							csec = dsec - xsec
						else
							csec = 0
						end
					end
				elseif (dday > xday and dmonth == xmonth) or (xday > dday and dmonth > xmonth) then -- next day
					chour = (24 - xhour) + dhour
					if xhour > dhour then
						if xmin > dmin then
							cmin = xmin - dmin
							compareSec()
						elseif xmin < dmin then
							cmin = dmin - xmin
							compareSec()
						else
							if xsec > dsec then
								csec = xsec - dsec
								cmin = 59
								chour = chour - 1
							elseif xsec < dsec then
								csec = dsec - xsec
							else
								csec = 0
							end
						end
					else -- same hour
						chour = 22
						if xmin > dmin then
							cmin = 60 - (xmin - dmin)
							compareSec()
							print("CHET")
						elseif xmin < dmin then
							cmin = dmin - xmin
							compareSec()
						else
							cmin = 59
							if xsec > dsec then
								csec = 60 - ( xsec - dsec )
							else
								_G.saleShopBool = false
							end
						end
					end
				end
				
				print("HOUR/S PASSED: " .. chour)
				print("MINS PASSED: " .. cmin)
				print("SEC PASSED: " .. csec)
			end
		end
	end
end

local function checkingSale()
	checkForSale = settings.get('checkForSale')
	if checkForSale == false or checkForSale == nil then
		print("<<<<<< CHECK FOR SALE BOOL IS FALSE/NIL. CHECKING FOR SALE")
		local sendNotif = false
		for i = 1, #mydata do 	
			if mydata[i].type == 3 and mydata[i].label == "Cream" and mydata[i].productName == "Bacon and Cheese Quiche" and mydata[i].locked == 0 and coinVal <= 100 then
				checkForSale = true
				settings.set('checkForSale', checkForSale)
				
				local t = os.date('*t')
				local u = t
				u.hour = u.hour + 1
				local uString = tostring(os.date("%Y-%m-%d/%H:%M:%S", os.time(u)))
				print("<<<<<< START TIME OF SALE: " .. uString)

				dateOfSale.sT = uString
				settings.set('dateOfSale', _G.dateOfSale)
				
				sendNotif = true
			end
		end
		
		if sendNotif then
			local notification_options = {
			   alert = "[24HR 50% SALE] Need more coins? Check the SHOP now!",
			   badge = 1
			}
			--> Schedule a local notification x seconds after quitting the app
			local notification = notifications.scheduleNotification( 3600, notification_options )
			
			local notification2_options = {
			   alert = "[LAST CHANCE] Only 1 hour remaining of our special offer! What are you waiting for?",
			   badge = 1
			}
			--> Schedule a local notification x seconds after quitting the app
			local notification2 = notifications.scheduleNotification( 82800, notification2_options )
			
			
			print("<<<<<< SCHEDULING NOTIFICATIONS")
			
			
		end
		
	end
end

function onSystemEvent( event ) 
	if event.type == "applicationStart" then
		return true
	elseif event.type == "applicationResume" then
		if _G.shopOpen and checkForSale then
			print("REFRESH")
			checkTime()
			
			if _G.saleShopBool then 
				native.setActivityIndicator(true)
				timer.performWithDelay(1000, function() composer.showOverlay( "_shop", {time = 500,  isModal = true } ) end )
				composer.hideOverlay("fade", 500 )
			end
		end
		native.setProperty( "applicationIconBadgeNumber", 0 ) -- resets notification when app is opened
		return true
	elseif event.type == "applicationSuspend" then
		suspendTime = os.time()
		checkingSale()
		return true
	elseif event.type == "applicationExit" then
		return true
	end
end

if ( launchArgs and launchArgs.notification ) then
	onNotification( launchArgs.notification )
end

Runtime:addEventListener( "notification", onNotification )
native.setProperty( "applicationIconBadgeNumber", 0 ) --> resets notification when app is opened
Runtime:addEventListener( "system", onSystemEvent )

print("HEIGHT: " .. display.contentHeight)
print("WIDTH: " .. display.contentWidth)

local fonts = native.getFontNames()
local count = 0

for i,fontname in ipairs(fonts) do -- Count the number of total fonts
    count = count+1
end

print( "FONT COUNT: " .. count )
local name = "KIMBERLEY"     -- part of the Font name we are looking for
name = string.lower( name )

for i, fontname in ipairs(fonts) do -- Display each font in the terminal console
    j, k = string.find( string.lower( fontname ), name )
    if( j ~= nil ) then
        print( "fontname = " .. tostring( fontname ) )
    end
end

local function main()
	settings.load() --> Read JSON file
	loadData() --> Load all saved data
	checkTime()
end 

main()

composer.gotoScene( "_mainMenu", "fade", 500)
