
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}

local scene = composer.newScene()



-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function handleReleaseEvent( event )
	if event.target.id == "close" then
		composer.hideOverlay("fade", 500 )
	end
end

-- create()
function scene:create( event )

    local sceneGroup = self.view
	font_PoppinsR = "res/fonts/Poppins-Regular.ttf"
	font_Abril = "res/fonts/AbrilFatface-Regular.ttf"
	font_PoppinsI = "res/fonts/PoppinsItalic.ttf"
	font_PoppinsB = "res/fonts/PoppinsBold.ttf"
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
		
	local messageBox = newImageRectNoDimensions("res/graphics/game/dialogWin.png")
	messageBox.x = display.contentCenterX
	messageBox.y = 140
	mainGroup:insert(messageBox)
	
	local titleText = display.newText("Word Cuisine",  display.contentCenterX, messageBox.y - 100, font_Abril, 24)
	titleText:setFillColor(0.2980,0.0784,0.0274)
	mainGroup:insert(titleText)
	
	local options = {
		text = "",
		x = display.contentCenterX,
		y = titleText.y + 20,
		font = font_PoppinsI,
		fontSize = 14,
		align = "center"
	}
	
	local verText = display.newText(options)
	verText.text = "version " .. _G.buildNumber
	verText:setFillColor(0.5607, 0.1764, 0.0078)
	mainGroup:insert(verText)
	
	local siteText = display.newText(options)
	siteText.text = "www.popsiclegames.com"
	siteText.y = verText.y + 14
	siteText:setFillColor(0.5607, 0.1764, 0.0078)
	mainGroup:insert(siteText)
	
	local optionsName = {
		parent = mainGroup,
		text = "",
		x = display.contentCenterX,
		font = font_PoppinsB,
		fontSize = 17,
		align = "center"
	}
	
	local optionsMusic = {
		parent = mainGroup,
		text = "",
		x = display.contentCenterX,
		font = font_PoppinsR,
		fontSize = 15,
		align = "center"
	}
	
	local function addTextNames(num,name,yPos, optionsName)
		local nameText = display.newText(optionsName)
		nameText.text = name
		nameText.y = yPos
		if num == 1 then
			nameText:setFillColor(0.2980,0.0784,0.0274)
		else
			nameText:setFillColor(0.5607,0.1764,0.0078)
			nameText.fontSize = 14
		end
		mainGroup:insert(nameText)
	end
	
	addTextNames(1,"James Chua", 		siteText.y + 30,  optionsName)
	addTextNames(1,"Vonn Constantino", 	siteText.y + 47,  optionsName)
	addTextNames(1,"Jed Cruz", 		siteText.y + 64,  optionsName)
	addTextNames(1,"Isa Enriquez",  siteText.y + 81,  optionsName)
	addTextNames(1,"Angelo Joseph Felismino", 			siteText.y + 98,  optionsName)
	addTextNames(1,"Erick Garayblas", 	siteText.y + 115, optionsName)
	addTextNames(1,"Abby Manuel", 		siteText.y + 132, optionsName)
	addTextNames(1,"Shellina Magarzo", 	siteText.y + 149, optionsName)
	addTextNames(1,"Kristine Tuting",  	siteText.y + 166, optionsName)
	                	
	addTextNames(2,"Music by:", 		siteText.y + 195, optionsMusic)
	addTextNames(2,"opengameart.org", 			siteText.y + 210, optionsMusic)
	addTextNames(2,"Cappelletti Show by Serena", 	siteText.y + 225, optionsMusic)
	addTextNames(2,"Giannini", 	siteText.y + 240, optionsMusic)
	
	local closeBtn = globals.createImageButton( "close",
					   messageBox.x + 100,  messageBox.y - 100,
						"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png",
						mainGroup, handleReleaseEvent)
	closeBtn.anchorX = 0 closeBtn.anchorY = 1	
	
	mainGroup.y = - 1000
	transition.to(mainGroup, {time = 1400, y = 0, transition = easing.inOutElastic})

end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
			composer.removeScene( "credits" )
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene