
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}

local scene = composer.newScene()



-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function handleReleaseEvent( event )
	local buttonID = event.target.id
	
	if buttonID == "yes" then
		local options =
		{
			iOSAppId = "1372002096",
			supportedAndroidStores = { "google", "samsung", "amazon" },
		}
		native.showPopup( "rateApp", options )
		composer.hideOverlay("fade", 500 )
		
		_G.rateDone = true
		settings.set("rateDone", _G.rateDone)
		
	elseif buttonID == "no" then
		composer.hideOverlay("fade", 500 )
	end
	
	_G.doRateUs = false
	settings.set('doRateUs', _G.doRateUs)
end

-- create()
function scene:create( event )

    local sceneGroup = self.view
	font_Header = "res/fonts/AbrilFatface-Regular.ttf"
	font_Sub = "res/fonts/Poppins-Regular.ttf"
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	
	local rectOverlay = display.newRect(display.contentCenterX, display.screenOriginY ,display.actualContentWidth + 20, 3000)
	rectOverlay.anchorX = 0.5
	rectOverlay:setFillColor(0.08,0,0, 0.7)
	backGroup:insert(rectOverlay)	
		
	local messageBox = newImageRectNoDimensions("res/graphics/menu/dogsPopUpBox.png")
	messageBox.x = display.contentCenterX
	messageBox.y = 150
	mainGroup:insert(messageBox)
	
	local titleOptions1 = {
		parent = mainGroup,
		text = "Are you",
		x = display.contentCenterX,
		y = messageBox.y - 65,
		width = messageBox.width - 80,
		font = font_Header,
		fontSize = 20,
		align = "center"
	}
	local titleOptions2 = {
		parent = mainGroup,
		text = "enjoying the",
		x = display.contentCenterX,
		y = messageBox.y - 45,
		width = messageBox.width - 80,
		font = font_Header,
		fontSize = 20,
		align = "center"
	}
	local titleOptions3 = {
		parent = mainGroup,
		text = "game?",
		x = display.contentCenterX,
		y = messageBox.y - 25,
		width = messageBox.width - 80,
		font = font_Header,
		fontSize = 20,
		align = "center"
	}
	
	local titleText = display.newText(titleOptions1)
	titleText:setFillColor(0.2980, 0.0784, 0.0274)
	local titleText2 = display.newText(titleOptions2)
	titleText2:setFillColor(0.2980, 0.0784, 0.0274)
	local titleText3 = display.newText(titleOptions3)
	titleText3:setFillColor(0.2980, 0.0784, 0.0274)
	
	local options1 = {
		parent = mainGroup,
		text = "Leave us a rating! We'd",
		x = display.contentCenterX,
		y = messageBox.y,
		width = messageBox.width - 90,
		font = font_Sub,
		fontSize = 13,
		align = "center"
	}
	local options2 = {
		parent = mainGroup,
		text = "really appreciate it.",
		x = display.contentCenterX,
		y = messageBox.y + 15,
		width = messageBox.width - 90,
		font = font_Sub,
		fontSize = 13,
		align = "center"
	}
	
	local descText1 = display.newText(options1)
	descText1:setFillColor(0.5607, 0.1764, 0.0078)
	local descText2 = display.newText(options2)
	descText2:setFillColor(0.5607, 0.1764, 0.0078)
	
	local yesBtn = globals.createImageButton( "yes",
				display.contentCenterX - 55, descText2.y + descText2.height + 10 ,
				"res/graphics/game/btn_YesUp.png", "res/graphics/game/btn_YesDown.png",
				mainGroup, handleReleaseEvent, 91, 36)
	
	local noBtn = globals.createImageButton( "no",
				display.contentCenterX + 55, descText2.y + descText2.height + 10,
				"res/graphics/game/btn_NoUp.png", "res/graphics/game/btn_NoDown.png",
				mainGroup, handleReleaseEvent, 91, 36)
	
	mainGroup.y = - 1000
	transition.to(mainGroup, {delay = 400, time = 1400, y = 0, transition = easing.inOutElastic})

end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
		parent = event.parent
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
			composer.removeScene( "popUp_rateUs" )
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene