
-- -----------------------------------------------------------------------------------
-- Game.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local inspect = require( "inspect") 
local ls = require("loadsave")
local puzzles = require("puzzles")
local json = require( "json" )

local scene = composer.newScene()
local displayGroups = {}
local panels = {}
_G.lastScene = "_game"
local isOnline
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


-- All display.contentCenterY and display.screenOriginY has been replaced by these two
local gameContentCenterY = display.contentCenterY
local gameScreenOriginY = display.screenOriginY

-- If the phone is an Iphone X then 
if onIphoneX then
	gameContentCenterY = display.contentCenterY + 25
	gameScreenOriginY = display.screenOriginY + 25	
end 


--== PARTICLE
local filePath = system.pathForFile( "SoupSplash.json" )
local f = io.open( filePath, "r" )
local fileData = f:read( "*a" )
f:close()
 
-- Decode the string
local emitterParams = json.decode( fileData )

--> Timers
local timerShake = nil
local timerShowRewardAd = nil
local timerEmphasis = nil

_G.rewardVideoComplete = false

--> Sounds
local popUp
local letterShuffle
local letterSelect
local wordFound
local showHint
local showPause 
local luckyBowl
local levelFinish 
local claimGold
local wordFound
local noWord
local extraWord
local dupWord
local coinBomb
local saveCoins

local giveHint

local availableChannel = audio.findFreeChannel()

_G.loadSoundTable()

local function audioLetterSelect()
	availableChannel = audio.findFreeChannel()
	audio.play(_G.soundTable["letterSelect"],{ channel=availableChannel })
end

function testNetworkConnection()
    local socket = require("socket")
    local test = socket.tcp()
    test:settimeout( 1000 )  -- Set timeout to 1 second
    local netConn = test:connect( "www.google.com", 80 )
    if ( netConn == nil ) then
        return false
    end
    test:close()
    return true
end

isOnline = testNetworkConnection() 


local timerShowAd

local function displayBannerAd( event )
	if(_G.ads == true) then
		if ( _G.admob.isLoaded( "banner" ) and _G.bannerVisible == false) then
			_G.admob.show( "banner" )
			_G.bannerVisible = true
			_G.bannerHeight = _G.admob.height()
			print ("BANNER: Showing Banner Ad [Level Selection]")
			print (_G.bannerHeight)
			if timerShowAd ~= nil then
				timer.cancel(timerShowAd)
				timerShowAd = nil
			end
		end
	end
end

if system.getInfo( "environment" ) == "simulator" then
else
	if isOnline then
		timerShowAd = timer.performWithDelay(1000, displayBannerAd, 0)
	end
end
	
local levelNum = composer.getVariable( "levelNum")
local treatName = composer.getVariable( "treatName")
local treatIngName = composer.getVariable( "treatIngName")
local levelName = composer.getVariable( "levelName")
local levelCount = composer.getVariable( "levelCount")
local menuName = composer.getVariable( "menuName")
local letterCount = composer.getVariable( "letterCount")
local dataIndex = composer.getVariable( "dataIndex")
local thisLevel = composer.getVariable( "thisLevel")

local colorFill = {r = 0, g = 0, b = 0}

local FB_MENU = 1
local FB_INGREDIENT = 1


-- change / add according to added levels/menu
--[[
if treatName == "Chili con Carne" then
	colorFill = {r = 142/255, g = 108/255, b = 41/255}
	FB_MENU = 1
elseif treatName == "Cream of Mushroom Soup" then
	colorFill = {r = 232/255, g = 180/255, b = 175/255}
	FB_MENU = 2
elseif treatName == "Spaghetti with Meatballs" then
	colorFill = {r = 0, g = 205/255, b = 198/255}
	FB_MENU = 3
elseif treatName == "Xiao Long Bao" then
	colorFill = {r = 148/255, g = 178/255, b = 1/255}
	FB_MENU = 4
elseif treatName == "Shepherd's Pie" then
	colorFill = {r = 232/255, g = 180/255, b = 175/255}
	FB_MENU = 7
elseif treatName == "Bacon and Cheese Quiche" then
	colorFill = {r = 200/255, g = 248/255, b = 200/255}
	FB_MENU = 5
elseif treatName == "Taco Salad" then
	colorFill = {r = 200/255, g = 0, b = 100/255}
	FB_MENU = 6
elseif treatName == "Gyudon" then
	colorFill = {r = 0, g = 24/255, b = 252/255}
	FB_MENU = 7
elseif treatName == "Chicken Curry" then
	colorFill = {r = 0/255, g = 178/255, b = 0}
	FB_MENU = 8
end --]]

--> change / add according to added levels/menu [FOR FIREBASE]
local levelNameMap =
{
	{"Beef", "Pepper"},
	{"Onion", "Chicken Broth", "Butter", "Cream", "Mushroom",},
	{"Tomato", "Garlic", "Parmesan Cheese", "Beef", "Pasta" },
	{"Pork", "Flour", "Ginger", "Wine", "Soy Sauce"},
	{"Egg", "Cream", "Parmesan Cheese", "Swiss Cheese", "Onion", "Bacon"},
	{"Tomato", "Lettuce", "Red Wine Vinegar", "Tortilla Chips", "Onion", "Cheddar Cheese", "Beef"},
	{"Butter", "Potato", "Flour", "Rosemary", "Thyme", "Garlic", "Lamb"},
	{"Green Onion", "Sake" ,"Soy Sauce", "Rice", "Red Ginger", "Egg", "Beef"},
	{"Coconut Milk", "Ginger" ,"Garlic", "Tomato Sauce", "Onion", "Curry Powder", "Chicken"}
}

--> for FIREBASE only / DONT DELETE
for i = 1, #levelNameMap[FB_MENU] do
	local levelIngredient = levelNameMap[FB_MENU][i]
	if levelIngredient == treatIngName then
		FB_INGREDIENT = i
		break
	end	
end

--> Ensures that Reward Video will only show when 3rd MENU is unlocked
if treatName == "Chili con Carne" or treatName == "Cream of Mushroom Soup" then
	print("REWARD AD INACTIVE")
	_G.rewardAdBool = false
else
	print("REWARD AD ACTIVE")
	_G.rewardAdBool = true
end

if thisLevel ~= nil then
	levelCount = 20 -- change this 
end

local levelOptions = {levelNum = levelNum, locked = false, complete = false, soupName = menuName, levelName = levelName}
print ("PUZZLE INFO: " .. treatName .. " " .. levelName .. " " .. levelNum)

local assembledWords = {}
local createData = true

local whichDataLevel  

for i = 1, #_G.dataLevels do
	if ( _G.dataLevels[i].soupName == menuName and _G.dataLevels[i].levelNum == levelNum and 
	_G.dataLevels[i].levelName == levelName) then
		whichDataLevel = i
		if _G.dataLevels[i].complete == false then
			assembledWords = ls.loadTable("WordSet.json")
			if ( assembledWords == nil) then 
				assembledWords = {} 
			end
		else
			_G.rewardAdBool = false
		end
	end
end

for i = 1, #_G.dataLevels do
	if _G.dataLevels[i].soupName == menuName and _G.dataLevels[i].levelNum == levelNum 
		and _G.dataLevels[i].levelName == levelName then
		createData = false
		break
	else
		createData = true
	end
end

if createData then
	table.insert(_G.dataLevels, levelOptions)
	print("LEVEL NOT EXISTING IN SAVE FILE. SAVING LEVEL!")
else
	print("LEVEL ALREADY EXIST IN SAVE FILE!")
end

local showHowToPlay = settings.get("showHowToPlay")
if showHowToPlay == nil then showHowToPlay = true end

local coinTxt = nil
	
local function saveCoinsSIMPLE()
	tempCoinVal = _G.coinVal
	coinTxt.text = tempCoinVal
	settings.set('coins', _G.coinVal)
end


-- create()
function scene:create( event ) 
	
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
	
	font = "res/fonts/Poppins-Regular.ttf"
		
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
	local soupGroup =  globals.createDisplayGroup( "soupGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local lettersGroup = globals.createDisplayGroup( "lettersGroup", displayGroups, sceneGroup )
    local wordGroup = globals.createDisplayGroup( "wordGroup", displayGroups, sceneGroup )
	local resultsGroup = globals.createDisplayGroup( "resultsGroup", displayGroups, sceneGroup )
	local luckyBowlGroup = globals.createDisplayGroup( "luckyBowlGroup", displayGroups, sceneGroup )
	local luckyBowlA = globals.createDisplayGroup( "luckyBowlA", displayGroups, sceneGroup )
	local luckyBowlB = globals.createDisplayGroup( "luckyBowlB", displayGroups, sceneGroup )
	local pauseGroup = globals.createDisplayGroup( "pauseGroup", displayGroups, sceneGroup )
	local howToPlayGroup = globals.createDisplayGroup( "howToPlayGroup", displayGroups, sceneGroup )
	local adNotifGroup = globals.createDisplayGroup( "adNotifGroup", displayGroups, sceneGroup )
	
	local claimBtn
	
    local top = gameScreenOriginY
    local left = display.screenOriginX
    local bottom = display.contentHeight - gameScreenOriginY
    local right = display.contentWidth - display.screenOriginX
    local mRand = math.random
	
	local platePos = gameScreenOriginY +250

    local letters = {}
    local formedword = {}
    local textRect = nil
    local prevValue = ""
    local assembledWord = ""
    local rotationAngle = 20
	
	local whichSet =  _G.mydata[dataIndex].whichPuzzleSet
	
    local puzzleTable = _G.puzzleTables
    local whichPuzzle = levelNum
    local mainWord = puzzleTable[whichSet][whichPuzzle][2]
    local extrawords = 0
    local wordstoguess = {}
    local wordsguessed = {}
    local wordsextra = {}
    local lettertiles = {}
    local disp_wordlist = {}
    local textscale = 1.0
    --> Added for Word Cuisine
    local puzzlestate = 4

    local coinsBtn = nil
    local pauseBtn = nil
    local wordBox = nil
    local maxPoints = 5
    local lineThickness = 7
    local endPoints = {}
    local touchEnd
    local timerShowAd
    local shuffling = false

    local plate = nil
    local wordsBtn = nil
	local hintBtn = nil
	local shuffleBtn = nil
	local coinRewardTxt = nil
	local coinIcon = nil
	local msg = nil
	local titleText = nil
	local textBoxImg = nil
	local rewardCoin = true
	local nextBtn = nil
	
	local tempCoinVal = _G.coinVal
	
	--> Lucky Bowl Variables
	local tempVal = 0
	local lockImg = nil 
	local checkImg = nil
	local unlocked = false
	local bar      = nil
	local barMsg   = nil
	local startBar = nil
	local midBar   = nil
    local endBar   = nil
	local foundExtraWords = {}
	
    --> Functions
    local assembleWord
    local resetAssembledWord
    local checkfromList
    local checkfromExtra
    local shakeEnvelope
	local shakeBack
	local initializeLuckyBowl
	local movePoint
	local acceptWord
	local saveStage
		
    --> Lay-out the word tiles depending on the puzzle
    extrawords = puzzleTable[whichSet][whichPuzzle][1]
    local wordcount = #puzzleTable[whichSet][whichPuzzle] - ( tonumber( extrawords ) + 1 ) --> Deduct extra word counter
    local indexctr = 1
    wordstoguess = {}
    lettertiles = {}
	
	if extrawords > 0 then
		for i = 1, #_G.dataLevels do
			if _G.dataLevels[i].soupName == menuName and _G.dataLevels[i].levelNum == levelNum and _G.dataLevels[i].levelName == levelName then
				if(_G.dataLevels[i].extraWords == nil) then
					foundExtraWords = {}
				else
					foundExtraWords = _G.dataLevels[i].extraWords
				end
				break
			end
		end
	end
	
	local wordCountVal 
	
    --> Store all words to guess
    for i = 1, wordcount do
        wordstoguess[i] = puzzleTable[whichSet][whichPuzzle][i + 1]
        wordsguessed[i] = 0
        indexctr = indexctr + 1
		wordCountVal = i
    end
    indexctr = indexctr + 1
    --> Store all extra words
    for i = 1, extrawords do
        wordsextra[i] = puzzleTable[whichSet][whichPuzzle][indexctr]
        indexctr = indexctr + 1
        print( "EXTRA WORD#" .. i .. ": " .. wordsextra[i] )
    end
    --> Sort wordstoguess array such that longest words are last
    local tempwords = {}
    local ctr = #wordstoguess
    for i = 1, #wordstoguess do
        tempwords[ctr] = wordstoguess[i]
        ctr = ctr - 1
    end
    for i = 1, #tempwords do
        wordstoguess[i] = tempwords[i]
    end
    if ( #wordstoguess <= 6 ) then --> Up to 6 words
        textscale = 0.8
    elseif ( #wordstoguess > 6 and #wordstoguess < 13 ) then --> Up to 12 words
        textscale = 0.7
    elseif ( #wordstoguess > 12 and #wordstoguess < 17 ) then
        textscale = 0.6
    elseif ( #wordstoguess > 16 and #wordstoguess < 19 ) then
        textscale = 0.5
    else
        textscale = 0.4
    end

    resetAssembledWord = function()
        for i = 1, string.len( assembledWord ) do
            --> Delete word first
            display.remove( formedword[i] )
            formedword[i] = nil
        end
        for i = 1, string.len( mainWord ) do
            letters[i].alreadytouched = false
			letters[i].alpha = 1
			-- letters[i].fill.effect = "filter.monotone"
			-- letters[i].fill.effect.r = colorFill.r
			-- letters[i].fill.effect.g = colorFill.g
			-- letters[i].fill.effect.b = colorFill.b
			-- letters[i].fill.effect.a = 1
        end
		
		touchEnd = 1
		while( #endPoints > 0 ) do
			table.remove(endPoints)
		end
		
        assembledWord = ""
        prevValue = ""
        prevLen = 0
        display.remove( textRect )
        textRect = nil
    end
	
	if isOnline then
		if system.getInfo( "environment" ) == "simulator" then
			-- if _G.rewardAdBool then
				-- print("TIMER REWARD AD START ----")
				-- timerShowRewardAd = timer.performWithDelay(60000, 
					-- function() 
						-- resetAssembledWord() 
						-- composer.showOverlay( "popUp_watchAd", { effect = "fade", time = 500,  isModal = true } ) 
					-- end)
			-- end
		else
			if _G.rewardAdBool and _G.admob.isLoaded( "rewardedVideo" ) then
				print("TIMER REWARD AD START ----")
				timerShowRewardAd = timer.performWithDelay(60000, 
					function() 
						resetAssembledWord() 
						composer.showOverlay( "popUp_watchAd", { effect = "fade", time = 500,  isModal = true } )  
					end)
			end
		end	
	end
	
	local function resetRewardTimer()
		if isOnline then
			if _G.rewardAdBool and _G.admob.isLoaded( "rewardedVideo" ) then
				if timerShowRewardAd ~= nil then
					print("Starting Reward-Timer Again")
					timer.cancel(timerShowRewardAd)
					timerShowRewardAd = nil
					timerShowRewardAd = timer.performWithDelay(60000, 
						function() 
							resetAssembledWord() 
							composer.showOverlay( "popUp_watchAd", { effect = "fade", time = 500,  isModal = true } ) 
						end)
				end
			else
				print("Reward Video has already been shown! Reward-Timer is not available.")
			end
		end
	end	
	
	local function disableBtns()
		wordsBtn:setEnabled(false)
		shuffleBtn:setEnabled(false)
		hintBtn:setEnabled(false)
	end
	
	local function enableBtns()
		wordsBtn:setEnabled(true)
		shuffleBtn:setEnabled(true)
		hintBtn:setEnabled(true)
		pauseBtn:setEnabled(true)		
		coinsBtn:setEnabled(true)
	end
	
	function saveCoins()
		coinTxt.text = tempCoinVal
		transition.to( coinTxt, {time = 300, size = 20, onComplete = function() transition.to( coinTxt, { time = 100, size = 15}) end } )
		_G.coinVal = tempCoinVal
		settings.set('coins', _G.coinVal)
	end
	
	local nextLevelNum
	
	function goToNextLevel()
		if nextLevelNum == nil then
			nextLevelNum = levelNum + 1
		else
			nextLevelNum = nextLevelNum + 1
		end
		
		-- Add to the add counter only if after the Chilli con Carne level
		if treatName ~= "Chili con Carne" then
			_G.adCounter = _G.adCounter + 1
		end
		
		composer.setVariable( "levelNum", nextLevelNum)	
		composer.gotoScene( "blank", { effect = "slideUp", time = 500 } )
	end
	
	local function handleReleaseEvent( event )	
		local id = event.target.id
		if event.phase == "began" then
		elseif event.phase == "moved" then
		elseif event.phase == "ended" then
			_G.audioButtonSelect()
			if id == "quit" then
				Runtime:removeEventListener( "touch", movePoint )
				composer.gotoScene( "levelSelect", { effect = "slideDown", time = 500 } )
				
			elseif id == "next" and nextBtn then
				Runtime:removeEventListener( "touch", movePoint )
				
				if(levelCount == levelNum) then
					composer.gotoScene( "menu", { effect = "slideLeft", time = 300 } )
				else
					timer.performWithDelay(500, goToNextLevel(), 1)
					nextBtn:setEnabled(false)
					--composer.gotoScene( "levelSelect", { effect = "slideLeft", time = 300 } )
				end
				
			elseif id == "coins" then
				composer.showOverlay( "_shop", { effect = "fade", time = 500,  isModal = true } )
				
			elseif id == "closeHTP" then	
				howToPlayGroup:removeSelf()
				howToPlayGroup = nil
				enableBtns()
				wordGroup.isVisible  = true
				lettersGroup.isVisible = true
				coinsBtn:setEnabled(true)
				pauseBtn:setEnabled(true)
				settings.set("showHowToPlay", false)
			end
		end
	end
	
	movePoint = function( event )
        --> Insert a new point into the front of the array
        table.insert( endPoints, 1, { x = event.x, y = event.y, line = nil } ) 

        --> Remove any excessed points
        if( #endPoints > maxPoints ) then 
            table.remove( endPoints )
        end

        for i,v in ipairs( endPoints ) do
            local line = display.newLine( v.x, v.y, event.x, event.y )
            line:setStrokeColor( 1, 1, 1, 0.5 )
            line.strokeWidth = lineThickness
            --mainGroup:insert( line )
            transition.to( line, { alpha = 0, strokeWidth = 0, onComplete = function( event ) line:removeSelf() line = nil end } )                
        end

        if ( event.phase == "ended" ) then
            touchEnd = 1
            while( #endPoints > 0 ) do
                table.remove(endPoints)
            end
        elseif ( event.phase == "began" ) then
            touchEnd = 0
        end
    end

    local function popBack( obj )
        transition.to( obj, { time = 50, xScale = 1.0, yScale = 1.0 } )
        --> Transfer new x to actual x
        obj.x = obj.newxpos
        obj.y = obj.newypos
        shuffling = false
    end

    local function putLettersBack( obj )
        transition.to( obj, { time = 300, delay = 100, x = obj.newxpos, y = obj.newypos, xScale = 1.1, yScale = 1.1, onComplete = popBack } )
    end

	local dontDoThisOnce = false
	
    local function jumbleLetters()
		if dontDoThisOnce then 
			availableChannel = audio.findFreeChannel()
			audio.play(_G.soundTable["letterShuffle"],{ channel=availableChannel })
		end
		dontDoThisOnce = true
        --> Get original position of each letter first
        for i = 1, #letters do
            letters[i].oldxpos = letters[i].x
            letters[i].oldypos = letters[i].y
        end
        --> Jumble array X times
        for i = 1, #letters do
            local index = mRand( #letters)
			while i == index do
				index = mRand( #letters)
			end
            if ( i ~= index ) then
                letters[i].newxpos = letters[index].oldxpos
                letters[i].newypos = letters[index].oldypos
				
                letters[index].newxpos = letters[i].oldxpos
                letters[index].newypos = letters[i].oldypos
				
                letters[i].oldxpos = letters[i].newxpos
                letters[i].oldypos = letters[i].newypos
				
                letters[index].oldxpos = letters[index].newxpos
                letters[index].oldypos = letters[index].newypos
                --print( letters[i].value .. " is switched with " ..  letters[index].value )
            end
        end
        --> Move letters towards the center of the soup
        for i = 1, #letters do
            transition.to( letters[i], { time = 300, x = plate.x, y = plate.y, rotation = letters[i].rotation + 360, xScale = 0.7, yScale = 0.7, onComplete = putLettersBack } )
			
        end
    end

    giveHint = function(hintPrice)
        local hintgiven = false
        local exitloops = false
        local ctr = 1

        while( not hintgiven and not exitloops ) do
            for i = 1, #wordstoguess do
                if ( ctr < string.len( wordstoguess[i] ) + 1 ) then
                    if( not disp_wordlist[i][ctr].isVisible ) then
                        for j = ctr, string.len( wordstoguess[i] ) do
                            disp_wordlist[i][j].isVisible = true						
                            hintgiven = true
                            break --> Exit loop prematurely
                        end
                    end
                end
                if ( hintgiven ) then
					availableChannel = audio.findFreeChannel()
					audio.play(_G.soundTable["showHint"],{ channel=availableChannel })
					_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "used_hint", item_id = "1"} )
					_G.coinVal = _G.coinVal - hintPrice
					saveCoinsSIMPLE()	
                    break --> Exit outer loop prematurely
                end
                if ( ctr > string.len( wordstoguess[#wordstoguess] ) ) then
                    exitloops = true
                end
            end
            ctr = ctr + 1
        end
		
    end
	
	local rectOverlay = display.newRect(display.screenOriginX, display.screenOriginY ,3000,3000)
	rectOverlay:setFillColor(0.08,0,0, 0.7)
	rectOverlay.isVisible = false
	
	local bgWin2 = newImageRectNoDimensions( "res/graphics/game/dialogWin.png" )
	globals.setBetterAnchor( bgWin2 )
	bgWin2.x = display.contentCenterX - bgWin2.width / 2
	bgWin2.y = display.contentCenterY - bgWin2.height / 2
	pauseGroup:insert( bgWin2 )
	


	local function hidePauseScreen(event)
		if event.phase == "began" then
		elseif event.phase == "ended" then
			availableChannel = audio.findFreeChannel()
			audio.play(_G.soundTable["HidePause"],{ channel=availableChannel })
			lettersGroup.isVisible = true
			luckyBowlGroup:insert(rectOverlay)
			rectOverlay:toBack()
			pauseBtn.isVisible = true
			titleText.isVisible = true
			textBoxImg.isVisible = true
			transition.to(pauseGroup, {time = 1500, y = -pauseGroup.height - 200, transition = easing.outElastic})
			timer.performWithDelay(1500, enableBtns())
		end
	end
	
	local resumeBtn = globals.createImageButton( "resume",
			display.contentCenterX, bgWin2.y + bgWin2.height - 95,
			"res/graphics/menu/btnResumeUp.png", "res/graphics/menu/btnResumeDown.png",
			pauseGroup, hidePauseScreen, 90, 40 )
	
	local quitBtn = globals.createImageButton( "quit",
			display.contentCenterX, resumeBtn.y + resumeBtn.height,
			"res/graphics/menu/btnQuitUp.png", "res/graphics/menu/btnQuitDown.png",
			pauseGroup, handleReleaseEvent, 90, 40)
	
	local function computePuzzleState()
		local state = 4
		local percentSolved = (1 - ( #assembledWords / #wordstoguess ) )
	
		if (percentSolved >= 0.66) then 
			state = 4
		elseif (percentSolved >= 0.33) then 
			state = 3
		elseif  (percentSolved > 0) then 
			state = 2
		else 
			state = 1
		end
		
		return state
	end
	
	local function showPauseScreen(event)
		if event.phase == "began" then
		elseif event.phase == "ended" then
			availableChannel = audio.findFreeChannel()
			audio.play(_G.soundTable["showPause"],{ channel=availableChannel })
			lettersGroup.isVisible = false
			wordGroup:insert(rectOverlay)
			rectOverlay:toBack()
			resumeBtn:setEnabled(false)
			pauseBtn.isVisible = false
			pauseGroup.isVisible = true
			titleText.isVisible = false
			textBoxImg.isVisible = false
			pauseGroup.y = -pauseGroup.height
			transition.to(rectOverlay, {time = 400, alpha = 0.8})
			transition.to(pauseGroup, {time = 1500, y = -20, transition = easing.outElastic})
			timer.performWithDelay(1500, function() resumeBtn:setEnabled(true) end)
			disableBtns()
			coinsBtn:setEnabled(false)
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "viewed_settings", item_id = "1"} )
		end
	end

    local bg = newImageRectNoDimensions( "res/graphics/game/bg_table_01.png" )
    bg.x = display.contentCenterX
    bg.y = gameContentCenterY + 200
    --bg.height = 580 -- ???
	
    backGroup:insert( bg )

    pauseBtn = globals.createImageButton( "pause", 
                        0, 0, "res/graphics/game/btnPauseUp.png", "res/graphics/game/btnPauseDown.png",
                        mainGroup, showPauseScreen, 62, 28)
    _G.anchor.Center( pauseBtn )
	pauseBtn.height = pauseBtn.height + 5
    pauseBtn.x = left + ( pauseBtn.contentWidth * 0.5 ) + 2
    pauseBtn.y = gameScreenOriginY + 22
    mainGroup:insert( pauseBtn )
	pauseBtn:setEnabled(false)
	
	wordBox = newImageRectNoDimensions( "res/graphics/game/wordBox.png" )
    _G.anchor.Center( wordBox )
    wordBox.x = display.contentCenterX
    wordBox.y = pauseBtn.y + ( wordBox.contentHeight * 0.5 ) + 20
    mainGroup:insert( wordBox )
	
    --plate = newImageRectNoDimensions( "res/graphics/game/soup_bowl_white.png" )
    plate = newImageRectNoDimensions( "res/graphics/game/bowl_" .. treatName .. ".png" )
    plate.x = display.contentCenterX
    plate.y = wordBox.y + 250
    soupGroup:insert( plate )
	platePos = plate.y
	
	--> Compute for puzzle progress - transferred to a function called computePuzzleState()
	
	--[[ old computation
	puzzlestate = math.ceil( 4 - ( ( ( #assembledWords / #wordstoguess ) * 100 ) / 34 ) )
	if ( puzzlestate == 0 ) then puzzlestate = 1 end -]]
	
	puzzlestate = computePuzzleState()
	print( "puzzle state: " .. puzzlestate )
	treat = newImageRectNoDimensions( "res/graphics/game/treats_" .. treatName .. puzzlestate .. ".png")
	treat.x = display.contentCenterX
    treat.y = platePos
    soupGroup:insert( treat )
	
	local holidayBool = false
	local boolCNY = false
	local tableName
	local matName
	local boxName
	
	local date = os.date("*t")
	local datetoday = tostring(date.year.."-"..string.format("%02d", date.month).."-"..string.format("%02d", date.day))
	print("Date Today: " .. datetoday)
	
	local pattern = "(%d+)%-(%d+)%-(%d+)"
	local xyear, xmonth, xday = datetoday:match(pattern)
	
	if xmonth == "12" or ( xmonth == "01" and xday <= "15") then -- CHRISTMAS
		holidayBool = true
		
		tableName = "bg_xmas_table_01.png"
		matName = "bg_xmas_mat_01.png"
		boxName = "box_xmas_01.png"
		print("<<<<<<<< LOADING CHRISTMAS SKINS")
	elseif xmonth == "02" and (xday >= "15" and xday <= "29" ) then -- CHINESE NEW YEAR (UPDATE THIS EVERY YEAR)
		holidayBool = true
		boolCNY = true
		
		tableName = "box_CNY_table_01.png"
		matName = "box_CNY_mat_01.png"
		boxName = "box_CNY_01.png"
		print("<<<<<<<< LOADING CHINESE NEW YEAR SKINS")
	end
	
	if not holidayBool then
		tableName = "bg_table_01.png"
		matName = "bg_mat_01.png"
		print("<<<<<<<< LOADING DEFAULT SKINS")
	end
	
	--special
	local bgTable = newImageRectNoDimensions( "res/graphics/game/" .. tableName )
    bgTable.x = display.contentCenterX
    bgTable.y = plate.y
	bgTable.width = display.actualContentWidth
    backGroup:insert( bgTable )
	
	local defaultBg = newImageRectNoDimensions( "res/graphics/game/bg_cloth_01.png" )
    defaultBg.x = display.contentCenterX
    defaultBg.y = wordBox.y - 45
	defaultBg.width = display.actualContentWidth
    backGroup:insert( defaultBg )
	defaultBg.anchorY = 0.5
		
	--special
	local bgMat = newImageRectNoDimensions( "res/graphics/game/" .. matName)
    bgMat.x = display.contentCenterX
    bgMat.y = plate.y
    backGroup:insert( bgMat )
	
	if holidayBool then
		-- special
		local bgWordBox = newImageRectNoDimensions( "res/graphics/game/" .. boxName)
		bgWordBox.x = display.contentCenterX
		bgWordBox.y = wordBox.y - 5
		backGroup:insert( bgWordBox )
	end
	
	
	if boolCNY then
		local clouds = newImageRectNoDimensions( "res/graphics/game/box_CNY_clouds_01.png")
		clouds.x = 21
		clouds.y = (wordBox.y + wordBox.height/2) - 11
		mainGroup:insert( clouds )
		
		local knot = newImageRectNoDimensions( "res/graphics/game/box_CNY_knot_01.png")
		knot.x = wordBox.x + wordBox.width/2
		knot.y = (wordBox.y - wordBox.height/2) + 50
		mainGroup:insert( knot )
	end
	
	textBoxImg = newImageRectNoDimensions("res/graphics/game/textbox.png")
	_G.anchor.Center( textBoxImg)
    
	titleText = display.newText(treatIngName .. " - " .. levelNum, pauseBtn.x + pauseBtn.width/2 + 15, pauseBtn.y, 0,0, font, 30)
	titleText.alpha = 0
	titleText.anchorX = 0
	titleText:setFillColor (0.1019,0.0235,0.0078)
	
	textBoxImg.y = pauseBtn.y
	textBoxImg.x = pauseBtn.x + pauseBtn.contentWidth + 41
	
	textBoxImg:scale (1.1,1)
	
	mainGroup:insert( textBoxImg)
	mainGroup:insert( titleText)
	
	soupGroup.y = 300
	
	lettersGroup.alpha  = 0
	transition.to (soupGroup, {delay = 500, time = 300, y = 0, onComplete = function() 
		transition.to (lettersGroup, {delay = 200, time = 400, alpha = 1}) end
	})
	
	-- local soupTransition
	
	-- local function spinSoup (timeNum, degree, effect)
		-- if soupTransition ~= nil then
			-- transition.cancel(soupImage)
		-- end
		-- soupTransition = transition.to( soupImage, { rotation = soupImage.rotation + degree, time = timeNum, transition = effect} )
	-- end
	
--[[	
    local spoon = newImageRectNoDimensions( "res/graphics/game/spoon.png" )
    spoon.x = right - ( spoon.contentWidth * 0.5 ) + 20
    spoon.y = plate.y - 40
    mainGroup:insert( spoon )
    ]]--
	local tempFSize = 16
	if string.len( titleText.text ) > 14 then
		tempFSize = 13
	end
	
	transition.to (titleText,{ time = 200, delay = 1000, alpha = 0.8, size = tempFSize, transition = easing.inQuart})
	print ("Length: ".. string.len( titleText.text ))
	

	
	local function changeGraphicsLB()
		barMsg.text = _G.extraWordsVal.."/30"
		if _G.extraWordsVal == 30 then
			unlocked = true
		else
			unlocked = false
		end
		
		if unlocked then
			luckyBowlA.isVisible = true
			luckyBowlB.isVisible = false
			claimBtn.isVisible = true
		else
			luckyBowlB.isVisible = true
			luckyBowlA.isVisible = false
			claimBtn.isVisible = false
		end
	end

	local function scaleBarLB()
		startBar.x =  bar.x - 130
		midBar.x = startBar.x + startBar.width 
		if(_G.extraWordsVal == 0) then
			midBar.xScale = 0.000001
		else
			midBar.xScale = (_G.extraWordsVal/3.10) * 1.1917
		end
		endBar.x = midBar.x + midBar.contentWidth
	end
	
	
	local function closeLuckyBowl()
		_G.audioButtonSelect()
		lettersGroup.isVisible = true
		luckyBowlGroup.isVisible = false
		enableBtns()
	end
	
	local ENVELOPE_BOOL = false
	local initLBOnce = false
	
	local function showLuckyBowl( event )
		if ( event.phase == "began" ) then
			if initLBOnce == false then
				initializeLuckyBowl()
				initLBOnce = true
			end
			changeGraphicsLB()
			ENVELOPE_BOOL = true
        elseif ( event.phase == "moved" ) then
        elseif ( event.phase == "ended" ) then
			if ENVELOPE_BOOL then
				scaleBarLB()
				lettersGroup.isVisible = false
				luckyBowlGroup.isVisible = true
				availableChannel = audio.findFreeChannel()
				audio.play(_G.soundTable["luckyBowl"],{ channel=availableChannel })
				disableBtns()
				hintBtn:setEnabled(false)
			end
			if(string.len(assembledWord) > 1) then
				acceptWord(event)
			end
			resetAssembledWord()
			ENVELOPE_BOOL = false
			coinsBtn:setEnabled(false)
			pauseBtn:setEnabled(false)
		end	
	end
	
	local function spawnCoinIconsLB()
		local coinIconA = newImageRectNoDimensions( "res/graphics/game/coinBig.png" )
		coinIconA.anchorX = 1
		coinIconA:scale(0.5,0.5)
		coinIconA.x = display.contentCenterX - 35
		coinIconA.y = gameContentCenterY
		local mul = 5

		transition.to( coinTxt, {time = 300, size = 20, onComplete = function() transition.to( coinTxt, { time = 100, size = 15}) _G.coinVal = _G.coinVal + mul coinTxt.text = _G.coinVal saveCoinsSIMPLE() end } )
		transition.to (coinIconA, {time = 300, x = coinsBtn.x - 40, y = coinsBtn.y, alpha = 0.5, yScale = 1, xScale = 1, onComplete = function() display.remove(coinIconA) coinIconA = nil end })
	end
	
	local function resetLuckyBowl( event )
		if ( event.phase == "began" ) then
        elseif ( event.phase == "moved" ) then
		elseif ( event.phase == "ended" ) then
		
			if(_G.extraWordsVal == 30) then
				_G.tempExVal = _G.tempExVal - 30
				tempCoinVal = tempCoinVal + 30
				_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "claim_bonusCoins", item_id = "1"} )
			end
			
			if(_G.tempExVal >= 30) then
				_G.extraWordsVal = 30
				
				timer.performWithDelay(700, function() 
					luckyBowlA.isVisible = true
					luckyBowlB.isVisible = false
				end)
			end
			
			if(_G.tempExVal < 30) then
				_G.extraWordsVal = _G.tempExVal
				timer.performWithDelay(700, function() 
					luckyBowlA.isVisible = true
					luckyBowlB.isVisible = false
					claimBtn.isVisible = false
				end)

				if timerShake then
					timer.cancel(timerShake)
				end
				
				barMsg.text = _G.extraWordsVal.."/30"
				timer.performWithDelay(100, spawnCoinIconsLB, 6)
				scaleBarLB()
				availableChannel = audio.findFreeChannel()
				audio.play(_G.soundTable["claimGold"],{ channel=availableChannel })
			end
			
			print("TOTAL VALUE LUCKYBOWL: ".. _G.extraWordsVal)
			settings.set("extraWordsVal",_G.extraWordsVal )
			settings.set("tempExVal",_G.tempExVal )
		end	
		
	end

	coinsBtn = widget.newButton({defaultFile = "res/graphics/game/btnCoinsUp.png", 
		overFile = "res/graphics/game/btnCoinsDown.png", x = 0, y = pauseBtn.y, onRelease = handleReleaseEvent,  id = "coins", width = 110, height = 32 })
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
    _G.anchor.Center( pauseBtn )
	mainGroup:insert (coinsBtn)
	
	if _G.saleShopBool then
		local saleImg = newImageRectNoDimensions("res/graphics/menu/uiSaleGameplay.png")
		saleImg.x = coinsBtn.x  + 48
		saleImg.y = coinsBtn.y - 10
		mainGroup:insert( saleImg )
		
		local goBack = function()
			--transition.to(saleImg, {time = 500, xScale = 1, yScale = 1, y =  coinsBtn.y - 10, transition = easing.outElastic})
			transition.to(saleImg, {time = 500, xScale = 1, yScale = 1, transition = easing.outElastic})
		end
		
		local doEmphasis = function()
			--transition.to(saleImg, {time = 500, xScale = 1.1, yScale = 1.1, y =  coinsBtn.y - 13, transition = easing.inElastic, onComplete = goBack})
			transition.to(saleImg, {time = 500, xScale = 1.1, yScale = 1.1, transition = easing.inElastic, onComplete = goBack})
		end

		doEmphasis()
		timerEmphasis = timer.performWithDelay(3000, doEmphasis, 0)
	end

	local textSettings =
	{
		text = tempCoinVal,
		x = coinsBtn.x - 10,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	coinTxt:setFillColor(0.1019,0.0235,0.0078)
	mainGroup:insert(coinTxt)
	
    wordsBtn = widget.newButton
    {
        defaultFile = "res/graphics/game/btnWordsUp.png",
        overFile = "res/graphics/game/btnWordsDown.png",
        x = plate.x - 120,
        y = plate.y - 70,
		id = "words",
        width = 62,
        height = 62,
        onEvent = showLuckyBowl
    }
	if _G.tempExVal  >= 30 then
			timerShake = timer.performWithDelay( 200, shakeEnvelope, 0 )
	end
    mainGroup:insert( wordsBtn )
	
	-- some animation effects when player gets an extra word
	local function extraWordsEffect()
		local effectIcon = newImageRectNoDimensions( "res/graphics/game/coinBig.png" )
		effectIcon.anchorX = 1
		effectIcon:scale(1,1)
		effectIcon.x = treat.x
		effectIcon.y = treat.y
		effectIcon.alpha = 1
		transition.to (effectIcon, {time = 300, x = wordsBtn.x, y = wordsBtn.y, alpha = 0.5, yScale = 0.5, xScale = 0.5, onComplete = function() display.remove(effectIcon) effectIcon = nil end })
	end

    shakeBack = function()
        transition.to( wordsBtn, { time = 50, rotation = 0 } )
    end

    shakeEnvelope = function()
        transition.to( wordsBtn, { time = 50, rotation = 5, onComplete = shakeBack } )
    end
	
	
	--======= SHOW LUCKY BOWL START
	
	
	local hamsterSheet1Options =
	{
		width = 261,
		height = 269,
		numFrames = 4
	}
	
	local hamsterSheet2Options =
	{
		width = 288,
		height = 299,
		numFrames = 2
	}
	
	local dogsSheet1Options = 
	{
		width = 324,
		height = 300,
		numFrames = 2
	}
	
	local dogsSheet2Options =
	{
		width = 324,
		height = 300,
		numFrames = 2
	}
	
	local sheet_hamster1 = graphics.newImageSheet( "res/graphics/game/hamster1SS.png", hamsterSheet1Options ) -- idle
	local sheet_hamster2 = graphics.newImageSheet( "res/graphics/game/hamster2SS.png", hamsterSheet2Options ) -- complete
	
	local sheet_dogs1 = graphics.newImageSheet( "res/graphics/game/spritesheet_01.png", dogsSheet1Options ) -- idle
	local sheet_dogs2 = graphics.newImageSheet( "res/graphics/game/spritesheet_02.png", dogsSheet2Options ) -- complete

	local sequences_hamster1 = {
		{
			name = "hamster1",
			start = 1,
			count = 4,
			time = 800,
			loopCount = 0,
			loopDirection = "forward"
		}
	}

	local sequences_hamster2 = {
		{
			name = "hamster2",
			start = 1,
			count = 2,
			time = 800,
			loopCount = 0,
			loopDirection = "forward"
		}
	}
	
	local sequences_dogs1 = {
		{
			name = "dogs1",
			start = 1,
			count = 2,
			time = 800,
			loopCount = 0,
			loopDirection = "forward"
		}		
	}
	
	local sequences_dogs2 = {
		{
			name = "dogs2",
			start = 1,
			count = 2,
			time = 800,
			loopCount = 0,
			loopDirection = "forward"
		}		
	}

	initializeLuckyBowl = function()
	
		rectOverlay.isVisible = true
		
		local bgExtra = newImageRectNoDimensions( "res/graphics/game/bg.png" )
		bgExtra.x = display.contentCenterX - bgExtra.width / 2
		bgExtra.y = gameContentCenterY - bgExtra.height / 2
		
		local cookie = newImageRectNoDimensions( "res/graphics/game/fortune-cookie-closed.png" )
		cookie.x = display.contentCenterX - 50
		cookie.y = bgExtra.y + bgExtra.height/2 - 10
		cookie.isVisible = false
		
		local cookieOpen = newImageRectNoDimensions( "res/graphics/game/fortune-cookie.png" )
		cookieOpen.x = display.contentCenterX - 50
		cookieOpen.y = bgExtra.y + bgExtra.height/2 - 5
		cookieOpen.isVisible = false

		
		globals.setBetterAnchor( rectOverlay )
		globals.setBetterAnchor( bgExtra )
		
		luckyBowlGroup:insert( rectOverlay )	
		luckyBowlGroup:insert( bgExtra )
		--luckyBowlB:insert( cookie )
		--luckyBowlA:insert( cookieOpen )
		
		bar = newImageRectNoDimensions( "res/graphics/game/barBg.png" )
		bar.x =  display.contentCenterX
		bar.y = cookie.y + cookie.height - 0
		
		barMsg = display.newEmbossedText(_G.extraWordsVal .. "/30", bar.x, bar.y + 0, font, 20)
		barMsg:setFillColor(1,1,1) 

		startBar = newImageRectNoDimensions("res/graphics/game/bar1.png")
		midBar   = newImageRectNoDimensions("res/graphics/game/bar2.png")
		endBar   = newImageRectNoDimensions("res/graphics/game/bar3.png")
		
		startBar.anchorX = 0
		midBar.anchorX = 0
		endBar.anchorX = 0

		startBar.y = bar.y
		midBar.y = bar.y
		endBar.y = bar.y

		luckyBowlGroup:insert( bar )
		luckyBowlGroup:insert( startBar )
		luckyBowlGroup:insert( midBar )
		luckyBowlGroup:insert( endBar )
		luckyBowlGroup:insert( barMsg )		
		
		local closeBtn = globals.createImageButton( "close",
					   bgExtra.x + bgExtra.width - 45,  bgExtra.y + 37,
						"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png",
						luckyBowlGroup, closeLuckyBowl, 38, 38)
		closeBtn.anchorX = 0 closeBtn.anchorY = 1
			
		claimBtn = globals.createImageButton( "claim",
					   display.contentCenterX,  bgExtra.y + bgExtra.height - 60,
						"res/graphics/game/btnClaimUp.png", "res/graphics/game/btnClaimDown.png",
						luckyBowlGroup, resetLuckyBowl, 90, 35)
						
		local msg = display.newEmbossedText("Find extra words to", bgExtra.x + 85 , cookie.y - 70, font, 20)
			msg:setFillColor(0.6,0,0)
			msg.anchorX = 0 msg.anchorY = 1
		
		local msg2 = display.newEmbossedText("get additional coins!", bgExtra.x + 85, cookie.y - 50, font, 20)
			msg2:setFillColor(0.6,0,0)
			msg2.anchorX = 0 msg2.anchorY = 1
		
		local floor = newImageRectNoDimensions("res/graphics/game/floor.png")
		floor.x =  display.contentCenterX
		floor.y = bgExtra.y + bgExtra.height - 190
			
		local hamster2 = display.newSprite(sheet_hamster2, sequences_hamster2 )
				globals.setBetterAnchor( hamster2 )
				hamster2.x = cookie.x + 30
				hamster2.y = cookie.y - cookie.height/2 + 30
				hamster2:scale(1/2,1/2)
				hamster2:play()
				hamster2.isVisible = false
		
		local hamster1 = display.newSprite(sheet_hamster1, sequences_hamster1 )
				globals.setBetterAnchor( hamster1 )
				hamster1.x = cookie.x + 30
				hamster1.y = cookie.y - cookie.height/2 + 30
				hamster1:scale(1/2,1/2)
				hamster1:play()
				hamster1.isVisible = false
				
		local dogs1 = display.newSprite(sheet_dogs1, sequences_dogs1 )
				dogs1.x = cookie.x + 50
				dogs1.y = cookie.y - cookie.height/2 + 105
				dogs1:scale(0.8,0.8)
				dogs1:play()
				
		local dogs2 = display.newSprite(sheet_dogs2, sequences_dogs2 )
				dogs2.x = cookie.x + 50
				dogs2.y = cookie.y - cookie.height/2 + 105
				dogs2:scale(0.8,0.8)
				dogs2:play()
		
		
		lockImg = newImageRectNoDimensions( "res/graphics/game/checklistBlank.png" )
		checkImg = newImageRectNoDimensions( "res/graphics/game/greenCheck.png" )
		
		lockImg.y = msg.y - 14
		lockImg.x = msg.x - lockImg.width
		checkImg.y = lockImg.y - 5
		checkImg.x = lockImg.x
		
		luckyBowlGroup:insert(lockImg)	
		luckyBowlGroup:insert(floor)
		
		luckyBowlA:insert(dogs2) -- complete
		luckyBowlA:insert(checkImg)
		
		luckyBowlB:insert(dogs1) -- idle
		
		luckyBowlGroup:insert(luckyBowlA)	
		luckyBowlGroup:insert(luckyBowlB)	
		luckyBowlGroup:insert(msg)
		luckyBowlGroup:insert(msg2)
		
		luckyBowlGroup.isVisible = false
	end
	
	--======= SHOW LUCKY BOWL END / SHOW POP-UPS GAME ===
	local gfxName = "delightful"
	local imgPopUps = nil
	local hamster3 = nil
	local randomPop = 1
	
	local showOnce = true
	local endsInPopUp = false
	
	
	local function showGFX()
		if showOnce then
			randomPop = math.random(7)
			if(randomPop == 1) then
				gfxName = "delectable"
			elseif(randomPop == 2) then
				gfxName = "delicious"
			elseif(randomPop == 3 ) then
				gfxName = "delightful"
			elseif(randomPop == 4 ) then
				gfxName = "fresh"
			elseif(randomPop == 5 ) then
				gfxName = "marvelous"
			elseif(randomPop == 6 ) then
				gfxName = "scrumptious"	
			elseif(randomPop == 7 ) then
				gfxName = "yummy"	
			end
			
			local addHeight = 0
			
			if bannerVisible == true then
				addHeight = 90
			else
				addHeight = 0
			end
			
			imgPopUps = newImageRectNoDimensions( "res/graphics/game/gfx/" .. gfxName .. "-shadow.png" )
			imgPopUps.x = display.contentCenterX
			imgPopUps.y = wordsBtn.y - 160
			imgPopUps.alpha = 0
			imgPopUps:scale(1/4,1/4)
			
			transition.to( imgPopUps, { xScale = 1, yScale = 1, alpha = 1, time = 800, transition = easing.inOutElastic ,onComplete = timer.performWithDelay(1200,
			function() transition.to( imgPopUps,{xScale = 1/4, yScale = 1/4, alpha = 0, time = 100, onComplete = 
				function() imgPopUps:removeSelf() imgPopUps = nil end})end) })
			--[[
			hamster3 = display.newSprite(sheet_hamster2, sequences_hamster2 )
			hamster3.x = display.contentCenterX
			hamster3.y = display.contentHeight + 140
			hamster3:scale(0.7,0.7)
			hamster3:play()	
				
			transition.to( hamster3, { y = display.contentHeight - addHeight, time = 200, delay = 300, onComplete = timer.performWithDelay(1500,
			function() transition.to( hamster3,{y = display.contentHeight + 140, time = 100, onComplete = 
				function() hamster3:removeSelf() hamster3 = nil end})end) 
			})--]]
			availableChannel = audio.findFreeChannel()
			timer.performWithDelay(300, function() audio.play(_G.soundTable["popUp"],{ channel=availableChannel }) end) 
			endsInPopUp = true
			
			timer.performWithDelay(1500, function() showOnce = false end)
		end
	end


    assembleWord = function( string )
        local sletter
        local totalwidth = 0
        local textscale = 0.9

        for i = 1, string.len( string ) do
            --> Delete word first
            display.remove( formedword[i] )
            formedword[i] = nil
            --> String handling time!
            sletter = string.sub( string, i, i )
            formedword[i] = {}
            formedword[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			
			--[[
			formedword[i].fill.effect = "filter.monotone"
			formedword[i].fill.effect.r = colorFill.r
			formedword[i].fill.effect.g = colorFill.g
			formedword[i].fill.effect.b = colorFill.b
			formedword[i].fill.effect.a = 0.8--]]

            _G.anchor.Center( formedword[i] )
            --> Assemble black rectangle behind word
            totalwidth = totalwidth + formedword[i].contentWidth
            display.remove( textRect )
            textRect = nil
            textRect = display.newRect( formedword[1].x - 15, formedword[1].y - 15, totalwidth, 40 )
            textRect:setFillColor( 0/255, 0/255, 0/255, 128/255 )
            textRect.xScale = textscale
            textRect.yScale = textscale
            textRect.x = display.contentCenterX
            textRect.y = platePos - 100
            _G.anchor.Center( textRect )
            mainGroup:insert( textRect )
        end
        --> Position letters based on rect
        for i = 1, string.len( string ) do
            formedword[i].xScale = textscale
            formedword[i].yScale = textscale
            formedword[i].x = ( display.contentCenterX - ( textRect.contentWidth * 0.5 ) + ( formedword[i].contentWidth * 0.5 ) ) + ( ( i - 1 ) * formedword[i].contentWidth )
            formedword[i].y = textRect.y
        end
        --> Make sure word is always on top
        for i = 1, string.len( string ) do
            wordGroup:insert( formedword[i] )
        end
    end

    checkfromList = function( whichword )
        local result = 0

        for i = 1, #wordstoguess do
            --if ( string.match( wordstoguess[i], whichword ) ~= nil ) then
            if ( wordstoguess[i] == whichword ) then
                --> Word found, get word position
                result = i
                break --> Exit for loop
            end
        end

        return result
    end

    checkfromExtra = function( whichword )
        local result = 0

        for i = 1, #wordsextra do
            --if ( string.match( wordsextra[i], whichword ) ~= nil ) then
            if ( wordsextra[i] == whichword ) then
                --> Word found, get word position
                result = i
                break --> Exit for loop
            end
        end

        return result
    end

	local correctWord = 0
	
	--============= RESULTS SCREEN START
	resultsGroup.isVisible = false
	
	--============= RESULTS SCREEN END / SOUND START

	local soundBtn
	local musicBtn
	local soundBool = _G.audios.sfx
	local musicBool = _G.audios.music
 	
	--> Changes image of buttons in menu
	local function changeStateSound(event)
		local trashGroup = globals.createDisplayGroup( "trashGroup", displayGroups, sceneGroup )
		local trashGroup = globals.getTableMemberWithID( "trashGroup", displayGroups )
		
		--> initializes the button according to saved data
		if soundBtn == nil and musicBtn == nil then
			if soundBool then
				soundBtn = globals.createImageButton( "soundOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeStateSound, 81, 31)
				for i = 2,32 do
					audio.setVolume(0.5, {channel=i})
				end
			else
				soundBtn = globals.createImageButton( "soundOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0, {channel=i})
				end
			end
			
			if musicBool then
				musicBtn = globals.createImageButton( "musicOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				audio.setVolume( 1, { channel=1 } )
			else
				musicBtn = globals.createImageButton( "musicOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				audio.setVolume( 0, { channel=1 } )
			end
		--> changes the state of the buttons
		else
			if(event.target.id == "soundOn") then
				trashGroup:insert(soundBtn)
				soundBtn = nil
				soundBtn = globals.createImageButton( "soundOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0, {channel=i})
				end
				soundBool = false
			elseif(event.target.id == "musicOn") then
				trashGroup:insert(musicBtn)
				musicBtn = nil
				musicBtn = globals.createImageButton( "musicOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				audio.setVolume( 0, { channel=1 } )
				musicBool = false
			elseif(event.target.id == "soundOff") then	
				trashGroup:insert(soundBtn)
				soundBtn = nil
				soundBtn = globals.createImageButton( "soundOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0.5, {channel=i})
				end
				soundBool = true
			elseif(event.target.id == "musicOff") then
				trashGroup:insert(musicBtn)
				musicBtn = nil
				musicBtn = globals.createImageButton( "musicOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeStateSound, 81, 31 )
				audio.setVolume( 1 , { channel=1 } )
				musicBool = true
			end
		end
	
		trashGroup:removeSelf()
		_G.audios.sfx = soundBool
		_G.audios.music = musicBool
		settings.set('sfx', soundBool)
		settings.set('music', musicBool)
	end
	--============= SOUND END / PAUSE SCREEN START	
	
	local pauseTxt = display.newText("PAUSED", display.contentCenterX, bgWin2.y + bgWin2.height - 270, font, 35)
	pauseTxt:setFillColor(0.7,0,0)
	pauseGroup:insert( pauseTxt )
	
	local sfxTxt = display.newText("SOUNDS", bgWin2.x + 40, pauseTxt.y + 40, font, 20)
	globals.setBetterAnchor( sfxTxt )
	sfxTxt:setFillColor(0,0,0)
	pauseGroup:insert( sfxTxt )
	
	changeStateSound()
	
	local sfxTxt2 = display.newText("MUSIC", bgWin2.x + 40, musicBtn.y - 17, font, 20)
	globals.setBetterAnchor( sfxTxt2 )
	sfxTxt2:setFillColor(0,0,0)
	pauseGroup:insert( sfxTxt2 )

	pauseGroup.isVisible = false
	
	local function spawnCoinIcons()
		local coinIconA = newImageRectNoDimensions( "res/graphics/game/coinBig.png" )
		coinIconA.anchorX = 0.5
		coinIconA:scale(0.5,0.5)
		coinIconA.x = coinIcon.x - 5
		coinIconA.y = coinIcon.y - 10
		local mul = 2
		if levelNum == levelCount then
			mul = 10
		end
		transition.to( coinTxt, {time = 300, size = 20, onComplete = function() transition.to( coinTxt, { time = 100, size = 15}) _G.coinVal = _G.coinVal + mul coinTxt.text = _G.coinVal end } )
		transition.to (coinIconA, {time = 300, x = coinsBtn.x - 40, y = coinsBtn.y, alpha = 0.5, yScale = 1, xScale = 1, onComplete = function() display.remove(coinIconA) coinIconA = nil end })
	end
	
	--============= PAUSE SCREEN END
	
	-- delay the setting the next button to enabled to prevent bugs wherein you can click the next button before it appears
	
	local function SetNextButtonEnabled()
		nextBtn:setEnabled(true) 
	end
	
	function showEndScreen() --coinsBtn.y + coinsBtn.height - 10
		local rectOverlay = display.newRect(display.contentCenterX, coinsBtn.y + coinsBtn.height - 80, display.actualContentWidth + 40, display.actualContentHeight + 40)
		rectOverlay.anchorY = 0
		rectOverlay.anchorX = 0.5
		rectOverlay:setFillColor(0.08, 0, 0, 0.7)
		wordGroup:insert( rectOverlay )
		rectOverlay:toFront()
		
		availableChannel = audio.findFreeChannel(2)	
		audio.play(_G.soundTable["levelEnd"],{ channel=availableChannel })
		
		local bgWin = newImageRectNoDimensions( "res/graphics/game/dialogWin.png" )
		globals.setBetterAnchor( bgWin )
		bgWin.x = display.contentCenterX - bgWin.width / 2
		bgWin.y = (display.contentCenterY - bgWin.height / 2) - 30
		resultsGroup:insert( bgWin )
		
		msg = newImageRectNoDimensions( "res/graphics/game/levelEnd.png" )
		globals.setBetterAnchor( msg )
		msg.x = bgWin.x + 15
		msg.y = bgWin.y + bgWin.width - msg.width/2 - 45
		msg.alpha = 1
		msg:scale(1, 1)
		resultsGroup:insert( msg )
		
		nextBtn = globals.createImageButton( "next",
				display.contentCenterX, msg.y + msg.width + 20,
				"res/graphics/game/btn_playNextUp.png", "res/graphics/game/btn_playNextDown.png",
				resultsGroup, handleReleaseEvent, 91, 36)
		nextBtn:setEnabled(false)
		resultsGroup:insert( nextBtn )
		
		local hamster4 
		local emitter
		local achievementText
		
		if( levelCount == levelNum) then
			timer.performWithDelay(690, function()
				if rewardCoin then
					timer.performWithDelay(150, function() 
						emitter = display.newEmitter( emitterParams )
						emitter.x = display.contentCenterX
						emitter.y = nextBtn.y + 125
						resultsGroup:insert( emitter )
						emitter:toFront()
						
						tempCoinVal = tempCoinVal + 60
						settings.set('coins', tempCoinVal)
						saveCoins()
					end)
					
					achievementText = display.newText(treatIngName .. " is Complete!", -500, msg.y + 10, font, 18)
					achievementText:setFillColor (0.3,0.5,0.1)
					resultsGroup:insert(achievementText)	
				
					hamster4 = display.newSprite(sheet_hamster2, sequences_hamster2 )
					hamster4.x = display.contentCenterX
					hamster4.y = display.contentHeight + 200
					hamster4:scale(0.7,0.7)
					hamster4:play()	
					hamster4.alpha = 0
					resultsGroup:insert( hamster4 )
					
					transition.to(achievementText, {delay = 400, time = 400, x = display.contentCenterX, transition = easing.inOutElastic, onComplete = function()
					transition.to(achievementText, {delay = 1400, time = 500, x = 1000, transition = easing.inOutElastic}) end})
					
					transition.to( hamster4, {delay = 100, y = achievementText.y + 115, time = 200, onComplete=function() hamster4:toFront() achievementText:toFront()  end})
				end
			end)
			coinRewardTxt = display.newText("10+50", nextBtn.x - 30, nextBtn.y - 40, font, 100 )
		else
			coinRewardTxt = display.newText("10",  nextBtn.x - 5 ,  nextBtn.y - 40, font, 100 )
			settings.set('coins', _G.coinVal + 10)
		end 
		coinRewardTxt.anchorX = 0
		coinRewardTxt.alpha = 0
		coinRewardTxt:setFillColor(0.5607,0.1764,0.0078)
		resultsGroup:insert( coinRewardTxt )
		
		coinIcon = newImageRectNoDimensions( "res/graphics/game/coinBig.png" )
		coinIcon.x = coinRewardTxt.x - 5
		coinIcon.y = coinRewardTxt.y
		coinIcon.anchorX = 1
		coinIcon.alpha = 0
		coinIcon.xScale = 3
		coinIcon.yScale = 3
		resultsGroup:insert( coinIcon )

		lettersGroup.isVisible = false
		resultsGroup.isVisible = true
		titleText.isVisible = false
		textBoxImg.isVisible = false
		pauseBtn.isVisible = false
		
		coinsBtn:setEnabled(false)
	
		nextBtn.alpha = 0
		
		wordGroup:insert(rectOverlay)
		rectOverlay:toBack()
		
		if rewardCoin == false then
			coinIcon.isVisible = false
			coinRewardTxt.isVisible = false
		end
		
		resultsGroup.y = -resultsGroup.height
		transition.to(resultsGroup, {time = 1500, y = -20, transition = easing.outElastic})
		transition.to(coinIcon, { time = 1200, xScale = 1, yScale = 1, alpha = 1, transition = easing.inOutElastic, onComplete =
			function()
				local mul = 5
				if levelNum ~= levelCount then
					if rewardCoin then timer.performWithDelay(100, spawnCoinIcons, mul) end
				end
				--transition.to(soupImage, {delay = 400, time = 300, alpha = 0})
			end
		})
		transition.to( coinRewardTxt, {time = 1800, size = 30, alpha = 1, transition = easing.inOutElastic})
		
		local delayNextBtn
		
		if rewardCoin then
			if levelCount == levelNum then
				delayNextBtn = 2500
			else
				delayNextBtn = 1900
			end
		else
			delayNextBtn = 500
		end
		
		if _G.dataLevels[whichDataLevel].complete == false  then
			timer.performWithDelay(delayNextBtn, saveStage)
		end
		
		transition.to( nextBtn, { delay = delayNextBtn, time = 400, alpha = 1, onComplete = function() 
					timer.performWithDelay(500, SetNextButtonEnabled()) end})
	end
	
	
	saveStage = function()
		_G.dataLevels[whichDataLevel].complete = true
		_G.rewardAdBool = true
		
		settings.set('rewardAdBool', _G.rewardAdBool)
		settings.set('dataLevels', _G.dataLevels)
		
		print(_G.dataLevels[whichDataLevel].complete)
		if levelNum == levelCount then

			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "FINISHED: " .. FB_MENU .. " - " .. FB_INGREDIENT, item_id = "1"} )
			print("FINISHED: " .. treatName .. " - " .. treatIngName)
			_G.mydata[dataIndex].complete = 1
			settings.set('mydata['.. dataIndex ..']', _G.mydata[dataIndex])
			if(_G.mydata[dataIndex + 1].type == 3 and _G.mydata[dataIndex + 1] ~= nil) then
				_G.mydata[dataIndex + 1].locked = 0
				settings.set('mydata['.. dataIndex + 1 ..']', _G.mydata[dataIndex + 1])
			else
				if _G.mydata[dataIndex + 4] ~= nil then
					_G.mydata[dataIndex + 3].locked = 0
					_G.mydata[dataIndex + 4].locked = 0
					settings.set('mydata[' .. dataIndex + 3 .. ']', _G.mydata[dataIndex + 3])
					settings.set('mydata[' .. dataIndex + 4 .. ']', _G.mydata[dataIndex + 4])		
					
					_G.doRateUs = true
					settings.set('doRateUs', _G.doRateUs)
				end
			end
		end	
	end
	
	for i = 1, #_G.dataLevels do
		if ( _G.dataLevels[i].soupName == menuName and _G.dataLevels[i].levelNum == levelNum and 
		_G.dataLevels[i].levelName == levelName) then
			whichDataLevel = i
		end
	end
	
	local doOnce = false
	
	if _G.tempExVal  >= 30 then
		print("SHAKE")
		timerShake =  timer.performWithDelay( 200, shakeEnvelope, 0 )
	end
	
	acceptWord = function(event)
		event.target.xScale = 1.0
		event.target.yScale = 1.0
		--> Remove letter colorwash
		for i = 1, string.len( mainWord ) do
			letters[i].alpha = 1
			--letters[i].fill.effect.a = 1
		end
		--> Check if word is in the list
		local wordpos = checkfromList( assembledWord )
		print(assembledWord .. " position: " .. wordpos )
		wordsguessed[wordpos] = 1
		if ( wordpos > 0 ) then --> word found!
			
			local checker = false
			local checker2 = false
			if( string.len( assembledWord ) == letterCount) then
				if assembledWords ~= nil then
					for z = 1, #assembledWords do
						if(assembledWords[z] == assembledWord) then
							checker = true
						end
					end
				end
				if(checker == false) then
					showGFX()
					table.insert( assembledWords, assembledWord )
					
					--> Compute for puzzle progress
					--[[
					puzzlestate = math.ceil( 4 - ( ( ( #assembledWords / #wordstoguess ) * 100 ) / 25 ) )
					if ( puzzlestate == 0 ) then puzzlestate = 1 end --]]
					
					puzzlestate = computePuzzleState()
					print( "puzzle state: " .. puzzlestate )
					display.remove( treat )
					treat = nil
					treat = newImageRectNoDimensions( "res/graphics/game/treats_" .. treatName .. puzzlestate .. ".png")
					treat.x = display.contentCenterX
				    treat.y = platePos
				    soupGroup:insert( treat )

					if _G.dataLevels[whichDataLevel].complete == false then
						ls.saveTable(assembledWords, "WordSet.json")
					end
					
					local delayVal = 200
					if showOnce == false then
						delayVal = 200
						availableChannel = audio.findFreeChannel(2)	
						timer.performWithDelay(400, function() audio.play(_G.soundTable["wordFound"],{ channel=availableChannel }) end)
						
					end
					
					timer.performWithDelay (delayVal, function() 
						local prevX, prevY, wordlen = lettertiles[wordpos].x, lettertiles[wordpos].y, lettertiles[wordpos].wordlen
						display.remove( lettertiles[wordpos] )
						lettertiles[wordpos] = nil
						lettertiles[wordpos] = {}
						lettertiles[wordpos] = newImageRectNoDimensions( "res/graphics/game/tile_Filled" .. wordlen .. ".png" )
						lettertiles[wordpos].alpha = 0
						lettertiles[wordpos]:scale(textscale,0.2)
						transition.to(lettertiles[wordpos], {time = 800, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
					
						if ( #wordstoguess < 7 ) then
							_G.anchor.Center( lettertiles[wordpos] )
						else
							_G.anchor.CenterLeft( lettertiles[wordpos] )
						end
						lettertiles[wordpos].x = prevX
						lettertiles[wordpos].y = prevY
						-- lettertiles[wordpos].xScale = textscale
						-- lettertiles[wordpos].yScale = textscale
						lettertiles[wordpos].guessed = true
						lettertiles[wordpos].wordlen = wordlen
						mainGroup:insert( lettertiles[wordpos] )
					end)
					
					for j = 1, string.len( assembledWord ) do
						disp_wordlist[wordpos][j].isVisible = true
						--disp_wordlist[wordpos][j].fill.effect.a = 1
						disp_wordlist[wordpos][j].alpha = 0
						disp_wordlist[wordpos][j]:scale(0.2,0.2)
						transition.to(disp_wordlist[wordpos][j], {time = 800, delay = delayVal, xScale = textscale, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
						lettersGroup:insert( disp_wordlist[wordpos][j] )
					end

					resetRewardTimer()
					
				else
					print("DUPLICATE: " .. assembledWord .. " ALREADY FOUND")
					availableChannel = audio.findFreeChannel()
					audio.play(_G.soundTable["dupWord"],{loops = 1, channel = availableChannel})
					transition.to(lettertiles[wordpos], {time = 200, alpha = 0.3, onComplete = function() transition.to(lettertiles[wordpos], {time = 200, alpha = 1, onComplete = 
						function() transition.to(lettertiles[wordpos], {time = 200, alpha = 0.3, onComplete = function() transition.to(lettertiles[wordpos], {time = 200, alpha = 1}) end }) end}) end})
					
					
				end
			else
				if assembledWords ~= nil then
					for z = 1, #assembledWords do
						if(assembledWords[z] == assembledWord) then
							checker2 = true
						end
					end
				end
				if checker2 == false then
					availableChannel = audio.findFreeChannel(2)
					timer.performWithDelay(300, function() audio.play(_G.soundTable["wordFound"],{ channel=availableChannel }) end)
					table.insert( assembledWords, assembledWord )

					--> Compute for puzzle progress
					
					--[[
					puzzlestate = math.ceil( 4 - ( ( ( #assembledWords / #wordstoguess ) * 100 ) / 25 ) )
					if ( puzzlestate == 0 ) then puzzlestate = 1 end--]]
					
					puzzlestate = computePuzzleState()
					print( "puzzle state: " .. puzzlestate )
					display.remove( treat )
					treat = nil
					treat = newImageRectNoDimensions( "res/graphics/game/treats_" .. treatName .. puzzlestate .. ".png")
					treat.x = display.contentCenterX
				    treat.y = platePos
				    soupGroup:insert( treat )

					for i = 1, #_G.dataLevels do
						if ( _G.dataLevels[i].soupName == menuName and _G.dataLevels[i].levelNum == levelNum and 
						_G.dataLevels[i].levelName == levelName) then
							if _G.dataLevels[i].complete == false then
								ls.saveTable(assembledWords, "WordSet.json")
							end
						end
					end
					
					endsInPopUp = false
					
					local prevX, prevY, wordlen = lettertiles[wordpos].x, lettertiles[wordpos].y, lettertiles[wordpos].wordlen
					display.remove( lettertiles[wordpos]  )
					lettertiles[wordpos] = nil
					lettertiles[wordpos] = {}
					lettertiles[wordpos] = newImageRectNoDimensions( "res/graphics/game/tile_Filled" .. wordlen .. ".png" )
					lettertiles[wordpos].alpha = 0
					lettertiles[wordpos]:scale(textscale,0.2)
					
					transition.to(lettertiles[wordpos], {time = 500, delay = 100, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
					
					if ( #wordstoguess < 7 ) then
						_G.anchor.Center( lettertiles[wordpos] )
					else
						_G.anchor.CenterLeft( lettertiles[wordpos] )
					end
					lettertiles[wordpos].x = prevX
					lettertiles[wordpos].y = prevY
					-- lettertiles[wordpos].xScale = textscale
					-- lettertiles[wordpos].yScale = textscale
					lettertiles[wordpos].guessed = true
					lettertiles[wordpos].wordlen = wordlen
					mainGroup:insert( lettertiles[wordpos] )
					
					for j = 1, string.len( assembledWord ) do
						disp_wordlist[wordpos][j].isVisible = true
						--disp_wordlist[wordpos][j].fill.effect.a = 1
						disp_wordlist[wordpos][j].alpha = 0
						disp_wordlist[wordpos][j]:scale(0.2,0.2)
						transition.to(disp_wordlist[wordpos][j], {time = 800, delay = 200, xScale = textscale, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
						lettersGroup:insert( disp_wordlist[wordpos][j] )
					end
					
					resetRewardTimer()
					 
				else
					print("DUPLICATE: " .. assembledWord .. " ALREADY FOUND")
					availableChannel = audio.findFreeChannel()
					audio.play(_G.soundTable["dupWord"],{loops = 1, channel = availableChannel})
					transition.to(lettertiles[wordpos], {time = 200, alpha = 0.3, onComplete = function() transition.to(lettertiles[wordpos], {time = 200, alpha = 1, onComplete = 
						function() transition.to(lettertiles[wordpos], {time = 200, alpha = 0.3, onComplete = function() transition.to(lettertiles[wordpos], {time = 200, alpha = 1}) end }) end}) end})
				end
			end
		else --> word not found!
			--> Check if word exists on extra list
			local wordAssembled = assembledWord
			resetAssembledWord()
			
			wordpos = checkfromExtra( wordAssembled )
			if ( wordpos > 0) then
				local addWord = true
				
				for i = 1, #foundExtraWords do
					if foundExtraWords[i] == wordAssembled then
						print("EXTRA WORD: "..wordAssembled .. " already exists in the table")
						addWord = false
					end
				end
				
				if addWord then
					table.insert(foundExtraWords, wordAssembled)
					availableChannel = audio.findFreeChannel()
					audio.play(_G.soundTable["extraWord"],{ channel=availableChannel })
					timer.performWithDelay( 200, shakeEnvelope, 4 )
					_G.tempExVal = _G.tempExVal + 5
					if(_G.extraWordsVal ~= 30) then
						if(_G.tempExVal <= 30) then
							_G.extraWordsVal = _G.tempExVal
						end
						--barMsg.text = _G.extraWordsVal.."/30"
					end
					print("EXTRA WORD: ".. wordAssembled .. " has been inserted into the extra words table")
					_G.dataLevels[whichDataLevel].extraWords = foundExtraWords
					
					-- Effect when entering an extra word
					timer.performWithDelay(100, extraWordsEffect, #wordAssembled)
					
					-- Shake extra words
					if _G.tempExVal  == 30 then
						timerShake = timer.performWithDelay( 200, shakeEnvelope, 0 )
					elseif  (_G.tempExVal < 30 and timerShake) then
						timer.cancel(timerShake)
					end
					
					timer.performWithDelay(1000, function() 
						settings.set('dataLevels', _G.dataLevels)
						settings.set('extraWordsVal',_G.extraWordsVal )
						settings.set('tempExVal',_G.tempExVal )
					end)
					
					resetRewardTimer()
				end
			else
				if(string.len( wordAssembled ) > 1) then
					availableChannel = audio.findFreeChannel()
					audio.play(_G.soundTable["noWord"],{ channel=availableChannel })
				end
			end
		end
		--> Check if all words have been guessed
		local totalwords = 0
		for i = 1, #wordstoguess do
			if ( wordsguessed[i] == 1 ) then
				totalwords = totalwords + 1
			end
		end

		if ( totalwords == #wordstoguess and doOnce == false) then
			if _G.rewardAdBool == true and _G.admob.isLoaded("rewardedVideo") then
				if timerShowRewardAd ~= nil then
					print ("Stage Complete! Stopping Reward-Timer.")
					timer.cancel(timerShowRewardAd)
					timerShowRewardAd = nil
				end
			end
			--- SAVE STAGE ---
			if _G.dataLevels[whichDataLevel].complete == true  then
				rewardCoin = false
			end
			settings.set('extraWordsVal',_G.extraWordsVal )
			settings.set('tempExVal',_G.tempExVal )
	
			disableBtns()
			
			if endsInPopUp then
				timer.performWithDelay(2100, showEndScreen)
			else
				timer.performWithDelay(1400, showEndScreen)
			end
			
			doOnce = true
			local emptyTable = {}
			ls.saveTable(emptyTable, "WordSet.json")
			ls.saveTable(_G.dataLevels, "dataLevels.json")
		end
	end

	if _G.extraWordsVal >= 30 then
	else if  (_G.extraWordsVal < 30 and timerShake) then
	end
	
	end
	
	local function loadLastSave()
		for i = 1, #assembledWords do
			local assembledWord = assembledWords[i]
			--> Check if word is in the list
			local wordpos = checkfromList( assembledWord )
			print( assembledWord .. " position: " .. wordpos )
			wordsguessed[wordpos] = 1
			
			local prevX, prevY, wordlen = lettertiles[wordpos].x, lettertiles[wordpos].y, lettertiles[wordpos].wordlen
			display.remove( lettertiles[wordpos] )
			lettertiles[wordpos] = nil
			lettertiles[wordpos] = {}
			lettertiles[wordpos] = newImageRectNoDimensions( "res/graphics/game/tile_Filled" .. wordlen .. ".png" )
			lettertiles[wordpos].alpha = 0
			lettertiles[wordpos]:scale(0.2,0.2)
			transition.to(lettertiles[wordpos], {time = 800, xScale = textscale, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
		
			if ( #wordstoguess < 7 ) then
				_G.anchor.Center( lettertiles[wordpos] )
			else
				_G.anchor.CenterLeft( lettertiles[wordpos] )
			end
			lettertiles[wordpos].x = prevX
			lettertiles[wordpos].y = prevY
			lettertiles[wordpos].xScale = textscale
			lettertiles[wordpos].yScale = textscale
			lettertiles[wordpos].guessed = true
			lettertiles[wordpos].wordlen = wordlen
			mainGroup:insert( lettertiles[wordpos] )
			
			for j = 1, string.len( assembledWord ) do
				disp_wordlist[wordpos][j].isVisible = true
				disp_wordlist[wordpos][j].alpha = 0
				disp_wordlist[wordpos][j]:scale(0.2,0.2)
				transition.to(disp_wordlist[wordpos][j], {time = 800, xScale = textscale, yScale = textscale, alpha = 1, transition = easing.inOutElastic})
				lettersGroup:insert( disp_wordlist[wordpos][j] )
			end
		end
	end
	
    local function touchBG( event )
        if ( event.phase == "began" ) then
        elseif ( event.phase == "moved" ) then
        elseif ( event.phase == "ended" ) then
            --> Remove letter colorwash
            for i = 1, string.len( mainWord ) do
                letters[i].alpha = 1
				--letters[i].fill.effect.a = 1
            end
			if(string.len(assembledWord) > 1) then
				acceptWord(event)
			end
			resetAssembledWord()
        end
    end
	bg:addEventListener( "touch", touchBG )
	bgMat:addEventListener( "touch", touchBG )
	bgTable:addEventListener( "touch", touchBG )
	defaultBg:addEventListener( "touch", touchBG )
	wordBox:addEventListener( "touch", touchBG )
	
	-- local function touchSoup (event)
		-- if ( event.phase == "began" ) then
        -- elseif ( event.phase == "moved" ) then
			-- spinSoup(750, 20, easing.outQuint)
        -- elseif ( event.phase == "ended" ) then
        -- end
	-- end
	-- soupImage:addEventListener( "touch", touchSoup )
	
	local HINT_BOOL = false
	
	
	local function hintAlert( event )
		if ( event.action == "clicked" ) then
			local i = event.index
			if ( i == 1 ) then
				-- Do nothing; dialog will simply dismiss
			elseif ( i == 2 ) then
				composer.showOverlay( "_shop", { effect = "fade", time = 500,  isModal = true } )
			end
		end
	end
	
    local function touchHint( event )
        if ( event.phase == "began" ) then
			HINT_BOOL = true
        elseif ( event.phase == "moved" ) then
        elseif ( event.phase == "ended") then
			if HINT_BOOL then
				print("HINT: " .. tempCoinVal)
				
				if _G.coinVal >= 25 then
					giveHint(25)
				else
					local alert = native.showAlert( "Need more coins?", "Oops, you don't have enough hint coins. Would you like to get more?", { "NO", "YES" }, hintAlert )
				end
				--> Remove letter colorwash
				for i = 1, string.len( mainWord ) do
					letters[i].alpha = 1
					--letters[i].fill.effect.a = 1
				end
			end
			if(string.len(assembledWord) > 1) then
				acceptWord(event)
			end
			resetAssembledWord()
			HINT_BOOL = false
        end
    end
	
	local SHUFFLE_BOOL = false
	
    local function touchShuffle( event )
        if ( event.phase == "began" ) then
			SHUFFLE_BOOL = true
        elseif ( event.phase == "moved" ) then
        elseif ( event.phase == "ended" ) then
			if SHUFFLE_BOOL then
				if ( not shuffling ) then
					shuffling = true
					jumbleLetters()
					--spinSoup(1150, 180, easing.outCirc)
				end
				--> Remove letter colorwash
				for i = 1, string.len( mainWord ) do
					letters[i].alpha = 1
					--letters[i].fill.effect.a = 1
				end
			end
			if(string.len(assembledWord) > 1) then
				acceptWord(event)
			end
			resetAssembledWord()
			SHUFFLE_BOOL = false
        end
    end
	
	if _G.bannerVisible and _G.ads then
		_G.bannerHeight = _G.admob.height()
		_G.bannerHeight = _G.bannerHeight/3

	    print("Admob Height: " .. _G.bannerHeight )
	end
	
	hintBtn = widget.newButton
    {
        defaultFile = "res/graphics/game/btnHintUp.png",
        overFile = "res/graphics/game/btnHintDown.png",
        x = plate.x - 120,
        y = plate.y + 80 - _G.bannerHeight,
        width = 62,
        height = 62,
        onEvent = touchHint
    }
    mainGroup:insert( hintBtn )

    shuffleBtn = widget.newButton
    {
        defaultFile = "res/graphics/game/btnShuffleUp.png",
        overFile = "res/graphics/game/btnShuffleDown.png",
        x = plate.x + 120,
        y = plate.y + 80 - _G.bannerHeight,
        width = 62,
        height = 62,
        onEvent = touchShuffle
    }
    mainGroup:insert( shuffleBtn )
	
    local function touchLetter( event )
		if(imgPopUps == nil) then
			if ( event.phase == "began" ) then
				audioLetterSelect()
				event.target.xScale = 1.0
				event.target.yScale = 1.0
				
				event.target.alpha = 0.4
				--event.target.fill.effect.a = 0.2
				
				prevValue = event.target.value
				assembledWord = assembledWord .. prevValue
				assembleWord( assembledWord )
				letters[event.target.id].alreadytouched = true
				
			elseif ( event.phase == "moved" ) then
				event.target.xScale = 1.0
				event.target.yScale = 1.0
				
				event.target.alpha = 0.4
				--event.target.fill.effect.a = 0.2
				
				if ( string.len( assembledWord ) < 7 ) then
					if ( letters[event.target.id].alreadytouched == false ) then
						letters[event.target.id].alreadytouched = true
						prevValue = event.target.value
						assembledWord = assembledWord .. event.target.value
						assembleWord( assembledWord )
						audioLetterSelect()
					end
				end
			elseif ( event.phase == "ended" ) then
			end
		end
    end
	
	local offset = 135
	--> This part positions the letters in the soup based on the letter count
	if ( string.len( mainWord ) == 3 ) then
		for i = 1, string.len( mainWord ) do
			local sletter = string.sub( mainWord, i, i )
			letters[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			if ( i == 1 ) then
				letters[i].x = display.contentCenterX
				letters[i].y = platePos + 100 - offset
			elseif ( i == 2 ) then
				letters[i].x = display.contentCenterX - 40
				letters[i].y = platePos + 160 -offset
			elseif ( i == 3 ) then
				letters[i].x = display.contentCenterX + 40
				letters[i].y = platePos + 160 -offset
			end
			
			--[[]
			letters[i].fill.effect = "filter.monotone"
			letters[i].fill.effect.r = colorFill.r
			letters[i].fill.effect.g = colorFill.g
			letters[i].fill.effect.b = colorFill.b
			letters[i].fill.effect.a = 0.8
			]]--
			
			letters[i].rotation = math.random( -rotationAngle, rotationAngle )
			letters[i].value = sletter
			letters[i].id = i
			letters[i].alreadytouched = false
			lettersGroup:insert( letters[i] )
			letters[i]:addEventListener( "touch", touchLetter )
		end
	elseif ( string.len( mainWord ) == 4 ) then
		for i = 1, string.len( mainWord ) do
			local sletter = string.sub( mainWord, i, i )
			letters[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			if ( i == 1 ) then
				letters[i].x = display.contentCenterX - 40
				letters[i].y = platePos + 104 - offset
			elseif ( i == 2 ) then
				letters[i].x = display.contentCenterX + 40
				letters[i].y = platePos + 104 - offset
			elseif ( i == 3 ) then
				letters[i].x = display.contentCenterX - 40
				letters[i].y = platePos + 170 - offset
			elseif ( i == 4 ) then
				letters[i].x = display.contentCenterX + 40
				letters[i].y = platePos + 170 - offset
			end
			
			--[[
			letters[i].fill.effect = "filter.monotone"
			letters[i].fill.effect.r = colorFill.r
			letters[i].fill.effect.g = colorFill.g
			letters[i].fill.effect.b = colorFill.b
			letters[i].fill.effect.a = 0.8
			]]--
			
			letters[i].rotation = math.random( -rotationAngle, rotationAngle )
			letters[i].value = sletter
			letters[i].id = i
			letters[i].alreadytouched = false
			lettersGroup:insert( letters[i] )
			letters[i]:addEventListener( "touch", touchLetter )
		end
	elseif ( string.len( mainWord ) == 5 ) then
		for i = 1, string.len( mainWord ) do
			local sletter = string.sub( mainWord, i, i )
			letters[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			if ( i == 1 ) then
				letters[i].x = display.contentCenterX
				letters[i].y = platePos + 80 - offset
			elseif ( i == 2 ) then
				letters[i].x = display.contentCenterX - 54
				letters[i].y = platePos + 122 - offset
			elseif ( i == 3 ) then
				letters[i].x = display.contentCenterX + 54
				letters[i].y = platePos + 122 - offset
			elseif ( i == 4 ) then
				letters[i].x = display.contentCenterX - 40
				letters[i].y = platePos + 180 - offset
			elseif ( i == 5 ) then
				letters[i].x = display.contentCenterX + 40
				letters[i].y = platePos + 180 - offset
			end
			
			--[[
			letters[i].fill.effect = "filter.monotone"
			letters[i].fill.effect.r = colorFill.r
			letters[i].fill.effect.g = colorFill.g
			letters[i].fill.effect.b = colorFill.b
			letters[i].fill.effect.a = 0.8
			]]--
			
			letters[i].rotation = math.random( -rotationAngle, rotationAngle )
			letters[i].value = sletter
			letters[i].id = i
			letters[i].alreadytouched = false
			lettersGroup:insert( letters[i] )
			letters[i]:addEventListener( "touch", touchLetter )
		end
	elseif ( string.len( mainWord ) == 6 ) then
		for i = 1, string.len( mainWord ) do
			local sletter = string.sub( mainWord, i, i )
			letters[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			if ( i == 1 ) then
				letters[i].x = display.contentCenterX
				letters[i].y = platePos + 74 - offset
			elseif ( i == 2 ) then
				letters[i].x = display.contentCenterX - 52
				letters[i].y = platePos + 110 - offset
			elseif ( i == 3 ) then
				letters[i].x = display.contentCenterX + 52
				letters[i].y = platePos + 110 - offset
			elseif ( i == 4 ) then
				letters[i].x = display.contentCenterX - 52
				letters[i].y = platePos + 160 - offset
			elseif ( i == 5 ) then
				letters[i].x = display.contentCenterX + 52
				letters[i].y = platePos + 160 - offset
			elseif ( i == 6 ) then
				letters[i].x = display.contentCenterX
				letters[i].y = platePos + 196 - offset
			end
			
			--[[
			letters[i].fill.effect = "filter.monotone"
			letters[i].fill.effect.r = colorFill.r
			letters[i].fill.effect.g = colorFill.g
			letters[i].fill.effect.b = colorFill.b
			letters[i].fill.effect.a = 0.8
			]]--
			
			letters[i].rotation = math.random( -rotationAngle, rotationAngle )
			letters[i].value = sletter
			letters[i].id = i
			letters[i].alreadytouched = false
			lettersGroup:insert( letters[i] )
			letters[i]:addEventListener( "touch", touchLetter )
		end
	elseif ( string.len( mainWord ) == 7 ) then
		for i = 1, string.len( mainWord ) do
			local sletter = string.sub( mainWord, i, i )
			letters[i] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
			if ( i == 1 ) then
				letters[i].x = display.contentCenterX
				letters[i].y = platePos + 74 - offset
			elseif ( i == 2 ) then
				letters[i].x = display.contentCenterX - 46
				letters[i].y = platePos + 102 - offset
			elseif ( i == 3 ) then
				letters[i].x = display.contentCenterX + 46
				letters[i].y = platePos + 102 - offset
			elseif ( i == 4 ) then
				letters[i].x = display.contentCenterX - 62
				letters[i].y = platePos + 150 - offset
			elseif ( i == 5 ) then
				letters[i].x = display.contentCenterX + 62
				letters[i].y = platePos + 150 - offset
			elseif ( i == 6 ) then
				letters[i].x = display.contentCenterX - 27
				letters[i].y = platePos + 194 - offset
			elseif ( i == 7 ) then
				letters[i].x = display.contentCenterX + 27
				letters[i].y = platePos + 194 - offset
			end
			
			--[[
			letters[i].fill.effect = "filter.monotone"
			letters[i].fill.effect.r = colorFill.r
			letters[i].fill.effect.g = colorFill.g
			letters[i].fill.effect.b = colorFill.b
			letters[i].fill.effect.a = 0.8
			]]--
			
			letters[i].rotation = math.random( -rotationAngle, rotationAngle )
			letters[i].value = sletter
			letters[i].id = i
			letters[i].alreadytouched = false
			lettersGroup:insert( letters[i] )
			letters[i]:addEventListener( "touch", touchLetter )
		end
		
	end

	
	
    --> Draw letter tiles and actual letters
    for i = 1, #wordstoguess do
        local wordlen = string.len( wordstoguess[i] )
        lettertiles[i] = {}
        lettertiles[i] = newImageRectNoDimensions( "res/graphics/game/tile_Empty" .. wordlen .. ".png" )
        if ( #wordstoguess < 7 ) then
            _G.anchor.Center( lettertiles[i] )
            lettertiles[i].x = display.contentCenterX
            lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * #wordstoguess + ( 2 * #wordstoguess - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) )
        elseif ( #wordstoguess > 6 and #wordstoguess < 13 ) then
            _G.anchor.CenterLeft( lettertiles[i] )
            if ( i < 7 ) then
                lettertiles[i].x = display.contentCenterX - 142
                lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * 6 + ( 2 * 6 - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) )
            else
                lettertiles[i].x = lettertiles[6].x + lettertiles[6].contentWidth + 5
                lettertiles[i].y = lettertiles[i - 6].y
            end
        elseif ( #wordstoguess > 12 and #wordstoguess < 17 ) then
            _G.anchor.CenterLeft( lettertiles[i] )
            if ( i < 9 ) then
                lettertiles[i].x = display.contentCenterX - 142
                lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * 8 + ( 2 * 8 - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) ) - 2
            else
                lettertiles[i].x = lettertiles[8].x + lettertiles[8].contentWidth + 10
                lettertiles[i].y = lettertiles[i - 8].y
            end
        elseif ( #wordstoguess > 16 and #wordstoguess < 19 ) then
            _G.anchor.CenterLeft( lettertiles[i] )
            if ( i < 10 ) then
                lettertiles[i].x = display.contentCenterX - 142
                lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * 9 + ( 2 * 9 - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) ) - 6
            else
                lettertiles[i].x = display.contentCenterX --lettertiles[9].x + lettertiles[9].contentWidth + 10
                lettertiles[i].y = lettertiles[i - 9].y
            end
        elseif ( #wordstoguess > 18 and #wordstoguess < 23 ) then
            _G.anchor.CenterLeft( lettertiles[i] )
            if ( i < 12 ) then
                lettertiles[i].x = display.contentCenterX - 142
                lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * 11 + ( 2 * 11 - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) ) - 10
            else
                lettertiles[i].x = display.contentCenterX --lettertiles[11].x + lettertiles[11].contentWidth + 10
                lettertiles[i].y = lettertiles[i - 11].y
            end
        else
            _G.anchor.CenterLeft( lettertiles[i] )
            if ( i < 12 ) then
                lettertiles[i].x = display.contentCenterX - 142
                lettertiles[i].y = wordBox.y - ( ( lettertiles[i].contentHeight * textscale * 11 + ( 2 * 11 - 1 ) ) * 0.5 ) + 16 + ( ( i - 1 ) * ( lettertiles[i].contentHeight * textscale + 2 ) ) - 10
            elseif ( i > 11 and i < 23 ) then
                lettertiles[i].x = lettertiles[11].x + lettertiles[11].contentWidth + 10
                lettertiles[i].y = lettertiles[i - 11].y
            else
                lettertiles[i].x = lettertiles[22].x + lettertiles[22].contentWidth + 10
                lettertiles[i].y = lettertiles[i - 22].y
            end
        end
        lettertiles[i].guessed = false
        lettertiles[i].wordlen = wordlen
        lettertiles[i].xScale = textscale
        lettertiles[i].yScale = textscale
        lettersGroup:insert( lettertiles[i] )
        --> Draw letter tiles too but hide it
        local totalwidth = 0
        disp_wordlist[i] = {}
        for j = 1, wordlen do
            --> String handling time!
            local sletter = string.sub( wordstoguess[i], j, j )
            disp_wordlist[i][j] = {}
            disp_wordlist[i][j] = newImageRectNoDimensions( "res/graphics/game/letters/ltr_" .. sletter .. ".png" )
            disp_wordlist[i][j].xScale = textscale
            disp_wordlist[i][j].yScale = textscale
			
			----- > LETTERS IN BOARDS
			
			if i % 2 == 0 then
				--[[
				disp_wordlist[i][j].fill.effect = "filter.monotone"
				disp_wordlist[i][j].fill.effect.r = 26/255
				disp_wordlist[i][j].fill.effect.g = 180/255`
				disp_wordlist[i][j].fill.effect.b = 192/255
				disp_wordlist[i][j].fill.effect.a = 1 --]]
				
				if boolCNY then
					--[[
					disp_wordlist[i][j].fill.effect.r = 255/255
					disp_wordlist[i][j].fill.effect.g = 25/255
					disp_wordlist[i][j].fill.effect.b = 0/255--]]
				end
				
			else
				--[[
				disp_wordlist[i][j].fill.effect = "filter.monotone"
				disp_wordlist[i][j].fill.effect.r = 255/255
				disp_wordlist[i][j].fill.effect.g = 67/255
				disp_wordlist[i][j].fill.effect.b = 130/255--]]
				
				if boolCNY then
					--[[
					disp_wordlist[i][j].fill.effect.r = 255/255
					disp_wordlist[i][j].fill.effect.g = 125/255
					disp_wordlist[i][j].fill.effect.b = 0/255--]]
				end
				
			end

            lettersGroup:insert( disp_wordlist[i][j] ) 			
			
            --> Assemble black rectangle behind word
            totalwidth = totalwidth + disp_wordlist[i][j].contentWidth
        end
        --> Position letters based on rect
        for j = 1, wordlen do
            if ( #wordstoguess < 7 ) then
                _G.anchor.Center( disp_wordlist[i][j] )
                disp_wordlist[i][j].x = ( display.contentCenterX - ( totalwidth * 0.5 ) + ( disp_wordlist[i][j].contentWidth * 0.5 ) ) + ( ( j - 1 ) * disp_wordlist[i][j].contentWidth )
            else
                _G.anchor.CenterLeft( disp_wordlist[i][j] )
                disp_wordlist[i][j].x = lettertiles[i].x + ( ( j - 1 ) * disp_wordlist[i][j].contentWidth )
            end
            disp_wordlist[i][j].y = lettertiles[i].y
            disp_wordlist[i][j].isVisible = false
        end
    end
	
	jumbleLetters()
	disableBtns()
	if #assembledWords >= 1 then
		timer.performWithDelay(500,loadLastSave)
	end 
	
	local closeHTPBtn
	
	local function showHowToPlayScreen()
		local rectOverlay = display.newRect(display.contentCenterX, gameScreenOriginY ,display.actualContentWidth + 20, 3000)
		rectOverlay.anchorX = 0.5
		rectOverlay:setFillColor(0.08,0,0, 0.7)
	
		local htpImg = newImageRectNoDimensions("res/graphics/game/howToPlay.png")
		htpImg.x = display.contentCenterX
		htpImg.y = - htpImg.height 
		htpImg.anchorY = 0.5
		
		closeHTPBtn = globals.createImageButton("closeHTP", htpImg.x + htpImg.width/2 - 25, htpImg.y - htpImg.height/2 + 28,
							"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png", 
							howToPlayGroup, handleReleaseEvent, 38, 38)
							
		local htpText = display.newText("HOW TO PLAY ?", display.contentCenterX, htpImg.y- htpImg.height/2 + 40, font, 30 )						
		htpText:setFillColor(0.8,0,0)
		
		local htpText2 = display.newText("Swipe to build a valid word!", display.contentCenterX, htpImg.y + 58, font, 15 )
		htpText2:setFillColor(0.5607,0.1764,0.0078)
							
		local htpText3 = display.newText("Use 'Shuffle' or 'Hint' if you are stuck.", display.contentCenterX, htpImg.y + 138, font, 13 )
		htpText3:setFillColor(0.5607,0.1764,0.0078)
		
		howToPlayGroup:insert(rectOverlay)
		howToPlayGroup:insert(htpImg)
		howToPlayGroup:insert(closeHTPBtn)
		--howToPlayGroup:insert(htpText)
		howToPlayGroup:insert(htpText2)
		howToPlayGroup:insert(htpText3)

		transition.to(howToPlayGroup, {delay = 100, time = 900, y = gameContentCenterY + 400, transition = easing.inOutElastic})
		disableBtns()
		-- wordGroup.isVisible  = false
		-- lettersGroup.isVisible = false
		coinsBtn:setEnabled(false)
		pauseBtn:setEnabled(false)
		
		shuffleBtn:setEnabled(false)
		wordsBtn:setEnabled(false)
		hintBtn:setEnabled(false)
		
	end
	
	if showHowToPlay then showHowToPlayScreen() else timer.performWithDelay(1000, enableBtns, 1) end
	timer.performWithDelay(4000, enableBtns)
	
	local function LineDraw()
		Runtime:addEventListener( "touch", movePoint )
	end
	
    --> Allow line drawing for letters
	timer.performWithDelay(1000, LineDraw())
end

local timerChecker

local checkAdComplete = {}
function checkAdComplete:timer(event)
	if _G.rewardVideoComplete and isOnline then
		giveHint(0)
		timerShowAd = timer.performWithDelay(1000, displayBannerAd, 0)
		timer.cancel(event.source)
	end

end

function scene:checkAdStatus()
	if isOnline then
		timerChecker = timer.performWithDelay(100, checkAdComplete, 0)
	end
end

function scene:updateCoins()
	saveCoinsSIMPLE()
end

function scene:showAds()
	if system.getInfo( "environment" ) == "simulator" then
	else
		if isOnline then
			timerShowAd = timer.performWithDelay(1000, displayBannerAd, 0)
		end
	end
end

-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase
	
	shakeEnvelope = function()
        transition.to( wordsBtn, { time = 50, rotation = 5, onComplete = shakeBack } )
    end

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase
	
    if ( phase == "will" ) then
		checkTime()
		if timerEmphasis ~= nil then
			if timerEmphasis.timerStart then
				timer.cancel(timerEmphasis)
				timerEmphasis = nil
			end
		end		
        -- Code here runs when the scene is on screen (but is about to go off screen)
		composer.removeScene( "_game" )

    elseif ( phase == "did" ) then
		
			--audio.stop(1)
			audio.stop(2)
		for s,v in pairs( _G.soundTable ) do
			audio.dispose( _G.soundTable[s] )
			_G.soundTable[s] = nil
		end
        -- Code here runs immediately after the scene goes entirely off screen
		
		if timerShake then
			timer.cancel(timerShake)
		end 
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
	print("BABYE")
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
