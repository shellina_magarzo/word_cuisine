
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}

local scene = composer.newScene()

local watchedAd = false


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function handleReleaseEvent( event )
	local buttonID = event.target.id
end

local function displayRewardAd( event )
	composer.hideOverlay("fade", 500 )

	if ( _G.admob.isLoaded( "rewardedVideo" )) then
		timer.performWithDelay(2000, function() _G.admob.show( "rewardedVideo" ) end)
		print ("REWARDED VIDEO: Showing Reward Ad [Game]")
		watchedAd = true
	end
end

local function handleReleaseEvent()
	composer.hideOverlay("fade", 500 )
end

-- create()
function scene:create( event )

    local sceneGroup = self.view
	font = "res/fonts/Poppins-Regular.ttf"
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	
	_G.admob.hide()
	print ("BANNER: Hiding Banner Ad [OVERLAY]")
	_G.bannerVisible = false
	
	_G.rewardAdBool = true
	settings.set('rewardAdBool', _G.rewardAdBool)
	
	local rectOverlay = display.newRect(display.contentCenterX, display.screenOriginY ,display.actualContentWidth + 20, 3000)
	rectOverlay.anchorX = 0.5
	rectOverlay:setFillColor(0.08,0,0, 0.7)
	backGroup:insert(rectOverlay)	
	
	local popUp = newImageRectNoDimensions( "res/graphics/menu/popUp.png" )
	popUp.x = display.contentCenterX
	popUp.y = display.contentCenterY
	mainGroup:insert( popUp )
	
	local watchBtn = globals.createImageButton( "watchReward",
					display.contentCenterX, popUp.y + 120,
					"res/graphics/menu/btnWatchVideoUp.png", "res/graphics/menu/btnWatchVideoDown.png",
					mainGroup, displayRewardAd, 89, 38)
					
	local closeBtn = globals.createImageButton( "close",
				display.contentCenterX + popUp.width/2 - 20, popUp.y - popUp.height/2 + 20, 
				"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png",
				mainGroup, handleReleaseEvent, 38, 38)
	--[[
	local mascotImg = newImageRectNoDimensions( "res/graphics/game/freeHintMasc.png" )
	mascotImg.x = display.contentCenterX 
	mascotImg.y = watchBtn.y - mascotImg.height/2 - 35
	mainGroup:insert( mascotImg )--]]
	--[[
	local adTxt = display.newText("Earn FREE hint by", display.contentCenterX + 65, display.contentCenterY - 90, 300, 0, font, 22)
	local adTxt2 = display.newText("watching an ad!", display.contentCenterX + 70, adTxt.y + 20, 300, 0, font, 22)
	adTxt:setFillColor(0.4,0,0)
	adTxt2:setFillColor(0.4,0,0)
	
	mainGroup:insert( adTxt )
	mainGroup:insert( adTxt2 )--]]
	
	mainGroup.y = - 1000
	transition.to(mainGroup, {delay = 400, time = 1400, y = 0, transition = easing.inOutElastic})
end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
		parent = event.parent
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
		composer.removeScene( "popUp_watchAd" )
		if watchedAd then
			parent:checkAdStatus()
		end
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene