---------------------------------
----------- Map Designs and Extra's Goes Here0
---------------------------------
local spine = require "spine-corona.spine"

local M = {}

local layerSpine
local spineAnimate

local num = 0

local spineTable = {}
local tblTimers = {}

M.addAttachments = function(combinedSkin,skinName) --grabs attachments from skinName and adds them to the default skin
	
	for k,v in pairs(skinName.attachments) do
		local slotIndex = v[1]
		local attachmentName = v[2]
		local attachment = v[3]
		combinedSkin:addAttachment(slotIndex, attachmentName, attachment)
	end
end




-- local copyItem
M.createSpine = function(table)
	num = num + 1
	local sNum = num
	-- local designGroup = display.newGroup()

	-- return designGroup
	local t = table
	
	-- This example shows simple usage of displaying a skeleton with queued animations.

	local json = spine.SkeletonJson.new()
	-- json.scale = 0.6
	local skeletonData = json:readSkeletonDataFile(t.locationJSON..t.jsonFileName..".json")

	local skeleton = spine.Skeleton.new(skeletonData)
	function skeleton:createImage (attachment)
		-- Customize where images are loaded.
		
		local image = newImageRectNoDimensions(t.location .. attachment.name .. ".png")
		return image
	end
	-- skeleton.group.x = display.contentWidth * 0.5
	-- skeleton.group.y = display.contentHeight * 0.9
	skeleton.group.x = t.x
	skeleton.group.y = t.y

	skeleton.group.xScale = 1
	if t.xScale then
		skeleton.group.xScale = t.xScale
	end

	skeleton.group.yScale = 1
	if t.yScale then
		skeleton.group.yScale = t.yScale
	end

	skeleton.flipX = false
	skeleton.flipY = false
	-- skeleton.debug = true -- Omit or set to false to not draw debug lines on top of the images.
	-- skeleton.debugAabb = true
	skeleton:setToSetupPose()

	local bounds = spine.SkeletonBounds.new()

	-- AnimationStateData defines crossfade durations between animations.
	local stateData = spine.AnimationStateData.new(skeletonData)
	-- stateData:setMix("walk", "jump", 0.2)
	-- stateData:setMix("jump", "run", 0.2)

	-- AnimationState has a queue of animations and can apply them with crossfading.
	local state = spine.AnimationState.new(stateData)
	-- state:setAnimationByName(0, "test")
	state:setAnimationByName(0, t.animationName, true)
	-- state:addAnimationByName(0, "jump", false, 3)
	-- state:addAnimationByName(0, "run", true, 0)
	local tT
	state.onStart = function (trackIndex)
		tT = trackIndex
		-- print(trackIndex.." start: "..state:getCurrent(trackIndex).animation.name)
		if t.onStart then
			t.onStart()
		end
	end
	state.onEnd = function (trackIndex)
		-- print(trackIndex.." end: "..state:getCurrent(trackIndex).animation.name)
		if t.onEnd then
			t.onEnd()
		end
	end
	state.onComplete = function (trackIndex, loopCount)
		print(trackIndex)
		if t.onComplete then

			t.onComplete()
		end
		-- print(trackIndex.." complete: "..state:getCurrent(trackIndex).animation.name..", "..loopCount)
	end
	state.onEvent = function (trackIndex, event)
		print(trackIndex.." event: "..state:getCurrent(trackIndex).animation.name..", "..event.data.name..", "..event.intValue..", "..event.floatValue..", '"..(event.stringValue or "").."'")
		if t.onEvent then
			t.onEvent(state:getCurrent(trackIndex).animation.name,event)
		end
	end

	local lastTime = 0
	local touchX = 999999
	local touchY = 999999
	-- local headSlot = skeleton:findSlot("head")

	local function getDeltaTime()
	    local temp = system.getTimer()  -- Get current game time in ms
		local dt = (temp-runtime) / (1000/60)  -- 60 fps or 30 fps as base
		runtime = temp  -- Store game time
		return dt
	end

	function skeleton.group:enterFrame(event)
		-- Compute time in seconds since last frame.

		self.totalTime = self.totalTime + (event.time - self.lastTime)
		self.lastTime = event.time
		
		-- print("TIMER "..sNum.." : ".. self.totalTime)
		-- print(event.time)

		local currentTime = self.totalTime / self.speed

		local delta = currentTime - lastTime
		lastTime = currentTime

		-- Bounding box hit detection.
		-- bounds:update(skeleton, true)
		-- if bounds:containsPoint(touchX, touchY) then
		-- 	headSlot.g = 0;
		-- 	headSlot.b = 0;
		-- else
		-- 	headSlot.g = 1;
		-- 	headSlot.b = 1;
		-- end

		-- Update the state with the delta time, apply it, and update the world transforms.
		state:update(delta)
		state:apply(skeleton)
		skeleton:updateWorldTransform()
	end


	--Remove Self Totally Removes itself
	function skeleton.group:removeSpine()
		Runtime:removeEventListener("enterFrame", self)
		self:removeSelf()
		self = nil
	end


	skeleton.group.totalTime = 0
	skeleton.group.lastTime = system.getTimer()
	skeleton.group.reST = 0


	if t.speed then
		skeleton.group.speed = t.speed
	else
		skeleton.group.speed = 1000
	end
	
	-- skeleton.group.enterFrame = table.initStart
	Runtime:addEventListener("enterFrame", skeleton.group)
	
	item = skeleton.group
	item.skeleton = skeleton
	item.state = state
	item.bounds = bounds
	item.stateData = stateData

	return item
end

M.removeLayers = function()

	-- spineTable = nil
	-- Runtime:removeEventListener("enterFrame", spineAnimate)
	display.remove( layerSpine )    
	for i = 1 ,#tblTimers do
		timer.cancel(tblTimers[i])
		tblTimers[i] = nil
	end
	-- tblTimers = nil
	layerSpine = nil
	-- item.group = nil
	item = nil


end

--Hides Layer
M.hideLayer = function()

	print("LAYER HIDDEN")
	layerSpine.isVisible = false

end

--Show Layer
M.showLayer = function()

	print("LAYER SHOWN")
	layerSpine.isVisible = true


end

M.createLayers = function()

	layerSpine = display.newGroup()

end

return M