local thisHeight
local thisWidth

local isIpad = ( string.find( system.getInfo("architectureInfo"), "iPad" ) ~= nil )  

--isIpad = true

if isIpad then
	thisWidth = 430
	thisHeight = 500
else
	thisWidth = 320
	thisHeight = 480
end	

application =
{
    content =
    {
        fps = 60,
		width = thisWidth,
		height = thisHeight,
        scale = "letterbox",
		shaderPrecision = "highp",
		xAlign = "center",
        yAlign = "center",
        imageSuffix =
        {
            ["@2x"] = 1.5
        }
    },
    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert"
            }
        }
    },
    license =
    {
        google =
        {
            --> Replace this with app-specific key from Google Play Dev dashboard
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhxAqzxWmZoZJGQsYnLxXyGCuxUhLSzxCzdCzIXRmpQYvi0IyX+qOgKrzvJc+vsflREguHVXz7gGClOHW6GygG0HqyntJS+fTM9/WhDVWUA8xjg1R9YpiJbjq6LReeVXnc5IVI+4zLGsNGy6ddrTeUgHC546xxcWRjlK5xiwqBA9gX65iVAhwQyHL+8N6nHUY/Ve9ThvrB3NVQDy0NQ4n7WfO41EwepJAsLnCIsVYUF7C8iGZ8KsHAvFEpZLqLYvS8opkPsc70NZXHZjU0ObIr8UF7gMOQ5FkM7C3RTwuPVxurRcMk4/pSaHR320zXW+nOwXE9qlfBm3Si1QiQnvG5wIDAQAB",
			policy = "serverManaged"
		},
    }
}