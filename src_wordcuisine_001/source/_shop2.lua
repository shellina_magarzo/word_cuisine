
-- -----------------------------------------------------------------------------------
-- Shop.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )

local displayGroups = {}
local purchaseBtns = {}
local scene = composer.newScene()

local parent

--> Variables used for in-game store

local appleProductList = 
{
	"word.search.cooking.food.iap1", -- 240   coins
	"word.search.cooking.food.iap2", -- 720   coins
	"word.search.cooking.food.iap3", -- 1,340 coins
	"word.search.cooking.food.iap4", -- 2,940 coins
	"word.search.cooking.food.iap5", -- 6,240 coins
	"word.search.cooking.food.iap6"  -- Remove Ads
}
local googleProductList = 
{
	"word.search.cooking.food.iap1b", 
	"word.search.cooking.food.iap2b", 
	"word.search.cooking.food.iap3b", 
	"word.search.cooking.food.iap4b", 
	"word.search.cooking.food.iap5b", 
	"word.search.cooking.food.iap6b"
}

local priceList = 
{
	"$0.495",
	"$1.495",
	"$2.495",
	"$4.995",
	"$9.995",
	"$0.995"
}

local store
local isOnline = false

if ( system.getInfo( "platformName" ) == "Android" ) then
   store = require( "plugin.google.iap.v3" )
elseif ( system.getInfo( "platformName" ) == "iPhone OS" ) then
   store = require( "store" )
end

local whichIAP
_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "viewed_shop", item_id = "1"} )

local top = display.screenOriginY
local left = display.screenOriginX
local bottom = display.contentHeight - display.screenOriginY
local right = display.contentWidth - display.screenOriginX

function testNetworkConnection()
    local socket = require("socket")
    local test = socket.tcp()
    test:settimeout( 1000 )  -- Set timeout to 1 second
    local netConn = test:connect( "www.google.com", 80 )
    if ( netConn == nil ) then
        return false
    end
    test:close()
    return true
end


local function saveCoins()
	coinTxt.text = _G.coinVal
	settings.set('coins', _G.coinVal)
end

--> Give Premium Pack
local function proceedWithPurchase()
	if ( whichIAP == 1 ) then
		_G.coinVal = _G.coinVal + 240
	elseif ( whichIAP == 2 ) then
		_G.coinVal = _G.coinVal + 720
	elseif ( whichIAP == 3 ) then
		_G.coinVal = _G.coinVal + 1340
	elseif ( whichIAP == 4 ) then
		_G.coinVal = _G.coinVal + 2940
	elseif ( whichIAP == 5 ) then
		_G.coinVal = _G.coinVal + 6240
	elseif ( whichIAP == 6 ) then
		_G.ads = false
		settings.set('ads', _G.ads)
	end
	saveCoins()
	_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "bought_IAP" .. whichIAP, item_id = whichIAP } )
end

local tempTable = {}
local productIdentifiers = {}


local function loadProductsCallback( event )
	-- > Used to sort Google Play IAP IN ORDER OF PRICES
	if _G.whichPlatform == "Android" then
		if event.products then
		  table.sort(event.products, function(a,b) 
			if a.priceAmountMicros then
				return ( tonumber(a.priceAmountMicros) < tonumber(b.priceAmountMicros) )
			  end
		  end)
		end
	end
	
    _G.validProducts = event.products
    _G.invalidProducts = event.invalidProducts
	
	if _G.whichPlatform == "Android" then
		for i = 1, #googleProductList do 
			if(_G.validProducts[i].productIdentifier == googleProductList[1]) then
				table.insert(tempTable,1, _G.validProducts[i])
				elseif(_G.validProducts[i].productIdentifier == googleProductList[2]) then
					table.insert(tempTable,2,_G.validProducts[i])
				elseif(_G.validProducts[i].productIdentifier == googleProductList[3]) then
					table.insert(tempTable,3,_G.validProducts[i])
				elseif(_G.validProducts[i].productIdentifier == googleProductList[4]) then
					table.insert(tempTable,4,_G.validProducts[i])
				elseif(_G.validProducts[i].productIdentifier == googleProductList[5]) then
					table.insert(tempTable,5,_G.validProducts[i])
				elseif(_G.validProducts[i].productIdentifier == googleProductList[6]) then
					table.insert(tempTable,6,_G.validProducts[i])
			end
		end
		_G.validProducts = tempTable
	end
	
	
	if _G.validProducts ~= nil then
		print("LOADING PRODUCTS")
        for i = 1, #_G.validProducts do
			print(_G.validProducts[i].title )
			print(_G.validProducts[i].description .. " DESCRIPTION HERE")
			print("PRICE: " .. _G.validProducts[i].localizedPrice )
			print("PRODUCT ID: " .. _G.validProducts[i].productIdentifier )
			purchaseBtns[i]:setLabel( _G.validProducts[i].localizedPrice )
        end
		_G.productsLoaded = true
		settings.set('validProducts', _G.validProducts)
	else
		_G.validProducts = {}
		print("THERE ARE NO VALID PRODUCTS")
    end
	
    for j = 1,#_G.invalidProducts do 
		print("INVALID PROD: " .. _G.invalidProducts[j] )
    end
	timer.performWithDelay(1000, showIAPList, 1 )
end
-------------------------------------------------------------------------------
-- Handler for all store transactions
-- This callback is set up by store.init()
-------------------------------------------------------------------------------
local function mystoreCallbackGoogle( event )
    -- Google IAP initialization event
    if ( event.name == "init" ) then
        if not ( event.transaction.isError ) then
          -- Perform steps to enable IAP, load products, etc.
          --> NOTE: "canLoadProducts" is not supported in Google Play Store
          if ( store.isActive ) then
            store.loadProducts ( googleProductList, loadProductsCallback )
          end
        else  -- Unsuccessful initialization; output error details
            print( event.transaction.errorType )
            print( event.transaction.errorString )
        end
    -- Store transaction event
    elseif ( event.name == "storeTransaction" ) then
		if event.transaction.state == "purchased" then
			proceedWithPurchase()
			native.showAlert( _G.gametitle, "Thank you for your support!", { "OK" } )
		elseif  event.transaction.state == "restored" then
			-- Reminder: your app must store this information somewhere
			-- Here we just display some of it
			_G.ads = false
			settings.set('ads', _G.ads)
			print("receipt", event.transaction.receipt)
			print("transactionIdentifier", event.transaction.transactionIdentifier)
			print("date", event.transaction.date)
			print("originalReceipt", event.transaction.originalReceipt)
			native.showAlert( _G.gametitle, "Your previous purchase was successfully restored. Happy playing!", { "OK" } )
			--> MISSING: Restoring an in-app item should be handled differently, this is only applicable for the REMOVE ADS IAP
		elseif event.transaction.state == "consumed" then
			print( "Transaction consumed!" )
			print( "product identifier", event.transaction.productIdentifier )
			print( "receipt", event.transaction.receipt )
			print( "transaction identifier", event.transaction.identifier )
			print( "date", event.transaction.date )
			print( "original receipt", event.transaction.originalReceipt )
			print( "original transaction identifier", event.transaction.originalIdentifier )
			print( "original date", event.transaction.originalDate )
		elseif event.transaction.state == "cancelled" then
		elseif event.transaction.state == "failed" then
			native.showAlert( _G.gametitle, "Transaction cancelled.", { "OK" } )
			native.showAlert( _G.gametitle, "Failed! " .. event.transaction.errorType .. " " .. event.transaction.errorString .. ".", { "OK" } )
		else
			native.showAlert( _G.gametitle, "Unknown Error!", { "OK" } )
		end
		-- Tell the store we are done with the transaction.
		-- If you are providing downloadable content, do not call this until
		-- the download has completed.
		store.finishTransaction( event.transaction )
		--native.setActivityIndicator( false )

		--> Make item available for purchase again
		if ( _G.whichPlatform == "Android" ) and whichIAP ~= 6 then
			timer.performWithDelay(5000, store.consumePurchase(_G.validProducts[whichIAP].productIdentifier))
		end
	end
end

-------------------------------------------------------------------------------
-- Handler for all store transactions
-- This callback is set up by store.init()
-------------------------------------------------------------------------------
local function mystoreCallback( event )
	if event.transaction.state == "purchased" then
		proceedWithPurchase()
		native.showAlert( _G.gametitle, "Thank you for your support!", { "OK" } )
	elseif  event.transaction.state == "restored" then
		-- Reminder: your app must store this information somewhere
		-- Here we just display some of it
		_G.ads = false
		settings.set('ads', _G.ads)
		print("receipt", event.transaction.receipt)
		print("transactionIdentifier", event.transaction.transactionIdentifier)
		print("date", event.transaction.date)
		print("originalReceipt", event.transaction.originalReceipt)
		native.showAlert( _G.gametitle, "Your previous purchase was successfully restored. Happy playing!", { "OK" } )
		--> MISSING: Restoring an in-app item should be handled differently, this is only applicable for the REMOVE ADS IAP
	elseif event.transaction.state == "consumed" then
		print( "Transaction consumed!" )
		print( "product identifier", event.transaction.productIdentifier )
		print( "receipt", event.transaction.receipt )
		print( "transaction identifier", event.transaction.identifier )
		print( "date", event.transaction.date )
		print( "original receipt", event.transaction.originalReceipt )
		print( "original transaction identifier", event.transaction.originalIdentifier )
		print( "original date", event.transaction.originalDate )
	elseif event.transaction.state == "cancelled" then
		native.showAlert( _G.gametitle, "Transaction cancelled.", { "OK" } )
	elseif event.transaction.state == "failed" then
		native.showAlert( _G.gametitle, "Failed! " .. event.transaction.errorType .. " " .. event.transaction.errorString .. ".", { "OK" } )
	else
		native.showAlert( _G.gametitle, "Unknown Error!", { "OK" } )
	end
	-- Tell the store we are done with the transaction.
	-- If you are providing downloadable content, do not call this until
	-- the download has completed.
	store.finishTransaction( event.transaction )
	--native.setActivityIndicator( false )
end

--> Init store once only!
local function setupStore()
	-- Connect to store at startup
	if system.getInfo( "targetAppStore" ) == "apple" then
		store.init ( "apple", mystoreCallback )
		if (store.canLoadProducts ) then
			store.loadProducts (appleProductList, loadProductsCallback )
		end
	elseif system.getInfo( "targetAppStore" ) == "google" then
		store.init ( mystoreCallbackGoogle )
		print("SHOP: GOOGLE PRODUCTS HAS BEEN LOADED.")
	else
		print( "In-app purchases are not supported on this system/device." )
	end
end

local function onCompleteRestore( event )
	_G.audioButtonSelect()
	store.restore()
	_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "restored_purchase", item_id = "1"} )

end

local function onCompletePurchase( event )
	_G.audioButtonSelect()
	whichIAP = event.target.id
	print("A button has been pressed. Beep boop boop beep.") 
	if(_G.validProducts ~= nil) then
		--> Purchase IAP item here!
		if store.canMakePurchases then
			--native.setActivityIndicator( true )
			if ( _G.whichPlatform == "Android" ) then
				store.purchase(_G.validProducts[whichIAP].productIdentifier  )
				print("IM BUYING THIS PACKAGE IN ANDROID")
			elseif ( _G.whichPlatform == "iPhone OS" ) then
				store.purchase(_G.validProducts[whichIAP].productIdentifier  )
				print("IM BUYING THIS PACKAGE IN IOS")
			end
		end
	end
end

local function handleReleaseEvent( event )
	_G.audioButtonSelect()
    local buttonID = event.target.id
	if buttonID == "back" then
		composer.hideOverlay("fade", 500 )
    end
end

local coinFilePath = "res/graphics/menu/coin.png"

local menuCardOptions =
{
	frames = 
	{ 
		{	-- frame 1 / crownFrame
			x = 0,
			y = - 0,
			width = 304,
			height = 22
		},
		{	-- frame 2 / topFrame (title i.e. Prep Cook/Apprentice)
			x = 0,
			y = 22,
			width = 304,
			height = 39
		},
		{	-- frame 3 / midFrame (ingredient)
			x = 0,
			y = 160,
			width = 304,
			height = 60
		},
		{	-- frame 4 / bottomFrame (endFrame)
			x = 0,
			y = 254,
			width = 304,
			height = 40
		}		
	},
	sheetContentWidth = 304,
	sheetContentHeight = 278
}

local shopData = {}
for i = 1, 10 do
 shopData[i] = {}
end

shopData[1] = { type = 1 }
shopData[2] = { type = 2 }
shopData[3] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins240.png",    value="240", desc1="A handful of", desc2="coins" }
shopData[4] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins720.png",  value="720",   desc1="A hanky of",   desc2="coins" }
shopData[5] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins1340.png",   value="1340",desc1="A medium",     desc2="bowl of coins" }
shopData[6] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins2940.png",   value="2940",desc1="A large bowl", desc2="of coins" }
shopData[7] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins6240.png", value="6240",  desc1="A sack of",    desc2="coins" }
shopData[8] = { type = 3, boolCoin = false, imgPath="res/graphics/menu/noAds.png", desc1="Remove Ads"}
shopData[9] = { type = 3, boolCoin = false}
shopData[10] = { type = 4 }

local function createIAPbuttons(i)
    local mainGroup = globals.getTableMemberWithID( "mainGroup", displayGroups )
	
	local defFile 
	if isOnline then
		defFile = "res/graphics/menu/btnUp.png"
	else
		defFile = "res/graphics/menu/btnUpGray.png"
	end
	
	local purchaseBtn = widget.newButton(
    {
        id = i,
        label = "BUY",
		font = font,
		fontSize = 12,
		labelColor = { default={ 1,1,1 }, over={ 0.8, 0.8, 0.8, 0.8 } },
        onRelease = onCompletePurchase,
		parentGroup = mainGroup,
		defaultFile = defFile,
		overFile =  "res/graphics/menu/btnDown.png"
    }	
)		
	if not isOnline then
		purchaseBtn:setEnabled(false)
	end
	
	purchaseBtn.anchorY = 1
	table.insert( purchaseBtns, purchaseBtn )
end

local function onRowRender( event )
	local mainGroup = globals.getTableMemberWithID( "mainGroup", displayGroups )
	local menuCardSheet = graphics.newImageSheet("res/graphics/menu/menu_card.png", menuCardOptions  )
	local row = event.row
	local id = event.row.index
	
	if (shopData[id].type == 1 ) then  --> top bar
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
	elseif ( shopData[id].type == 2 ) then  --> title
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
		local options = {
			text = "Coin Shop",
			font = font,
			fontSize = 25,
		}
		local titleObj = display.newText( options )
		titleObj.x = display.contentCenterX
		titleObj.y = bgrow.y + 14
		titleObj:setFillColor(0,0.2,0)
		row:insert( titleObj )
	elseif ( shopData[id].type == 3 ) then  --> regular row
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
		
		local imgPack
		if shopData[id].imgPath ~= nil then
			imgPack = newImageRectNoDimensions(shopData[id].imgPath)
			imgPack.x = 70
			imgPack.y = bgrow.contentHeight * 0.5
			row:insert( imgPack )
		end
		
		if shopData[id].boolCoin then
			local coinImg = newImageRectNoDimensions(coinFilePath)
			coinImg.x = 120
			coinImg.y = imgPack.y - 13
			row:insert( coinImg )
			
			local options = {
				text = shopData[id].value,
				font = font,
				fontSize = 18,
			}
			
			local titleObj = display.newText( options )  	
			titleObj.x = 135
			titleObj.y = coinImg.y + 4
			titleObj:setFillColor(0,0.4,0)
			titleObj.anchorX = 0
			row:insert( titleObj )
			
			local desc1Options = {
				text = shopData[id].desc1,
				font = font,
				fontSize = 14,
			}
			
			local msgDesc1 = display.newText( desc1Options )  
			msgDesc1.x = 115
			msgDesc1.y = titleObj.y + 15
			msgDesc1.anchorX = 0
			msgDesc1:setFillColor(0.2,0.5,0.2)
			row:insert( msgDesc1 )
			
			local desc2Options = {
				text = shopData[id].desc2,
				font = font,
				fontSize = 14,
			}
			
			local msgDesc2 = display.newText( desc2Options )  
			msgDesc2.x = 115
			msgDesc2.y = msgDesc1.y + 15
			msgDesc2.anchorX = 0
			msgDesc2:setFillColor(0.2,0.5,0.2)
			row:insert( msgDesc2 )
			
			local button = purchaseBtns[id-2]
			button.y = imgPack.y + 20
			button.x = 245
			row:insert( button )
			
			if ( system.getInfo( "environment" ) == "simulator" ) then
				if ( id-2 == 1 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 2 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 3 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 4 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 5 ) then
					button:setLabel(priceList[id-2])
				end
			else
				if isOnline then
					print(_G.validProducts[id-2].localizedPrice)
					button:setLabel( _G.validProducts[id-2].localizedPrice )
				else
					if _G.validProducts == nil then
						button:setLabel(priceList[id-2])
					else
						button:setLabel( _G.validProducts[id-2].localizedPrice )
					end
				end
			end
		end
		
		if id == 8 then
			local desc1Options = {
				text = shopData[id].desc1,
				font = font,
				fontSize = 16,
			}
			
			local msgDesc1 = display.newText( desc1Options )  
			msgDesc1.x = 105
			msgDesc1.y = imgPack.y  + 5
			msgDesc1.anchorX = 0
			msgDesc1:setFillColor(0.5,0.2,0.2)
			row:insert( msgDesc1 )
			
			if _G.ads == true then
				local button = purchaseBtns[id-2]
				button.y = imgPack.y + 20
				button.x = 245
				row:insert( button )
			
				--> Show something when in simulator mode
				if ( system.getInfo( "environment" ) == "simulator" ) then
					if ( id-2 == 6 ) then
						purchaseBtns[id-2]:setLabel(priceList[id-2])
					end
				else --> Show localized prices on device mode
					if isOnline then
						purchaseBtns[id-2]:setLabel( _G.validProducts[id-2].localizedPrice )
					else
						if _G.validProducts == nil then
							purchaseBtns[id-2]:setLabel(priceList[id-2])
						else
							purchaseBtns[id-2]:setLabel( _G.validProducts[id-2].localizedPrice )
						end
					end
				end
				
			else
				local msgPurchased = display.newEmbossedText("Purchased!", msgDesc1.x + 100, 30, font, 16)
				msgPurchased:setFillColor(0.9,0.4,0.4,1)
				msgPurchased.rotation = -20
				globals.setBetterAnchor(msgPurchased)
				row:insert(msgPurchased)
			end
		elseif id == 9 then
			local defFile 
			
			if isOnline then
				defFile = "res/graphics/menu/btnRestorePurchasesUp.png"
			else
				defFile = "res/graphics/menu/btnRestorePurchasesUpGray.png"
			end
			
			local restoreBtn = globals.createImageButton( "restorePurchase",
						display.contentCenterX,  30,
						defFile, "res/graphics/menu/btnRestorePurchasesDown.png",
						mainGroup, onCompleteRestore )
			if not isOnline then
				restoreBtn:setEnabled(false)
			end			
			row:insert(restoreBtn)
		end
		
	elseif ( shopData[id].type == 4 ) then  --> bottom bar
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
	end
end

local tableView



-- create()
function scene:create( event )

	local sceneGroup = self.view
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local popUpGroup = globals.createDisplayGroup( "popUpGroup", displayGroups, sceneGroup )
   
	font = "res/fonts/BalooTammudu-Regular.ttf"

	isOnline = testNetworkConnection()
	print (isOnline)
	_G.admob.hide()
	_G.bannerVisible = false
	
    local bgTop = newImageRectNoDimensions( "res/graphics/menu/menu_top_02.png" )
    globals.setBetterAnchor( bgTop )
    bgTop.x = display.screenOriginX
    bgTop.y = display.screenOriginY - 3
	
	-- only if iphone X
	if onIphoneX then
		bgTop.height = 80 
	end
	
    mainGroup:insert( bgTop )
	
	local bgPaper = newImageRectNoDimensions( "res/graphics/menu/bgPaper.png" )
    globals.setBetterAnchor( bgPaper )
    bgPaper.x = display.contentCenterX - bgPaper.width/2
    bgPaper.y = bgTop.y + bgTop.contentHeight + 10 --remove noticable padding
	
	bgPaper.isVisible = false
	
	local bgBottom = newImageRectNoDimensions( "res/graphics/menu/bg_02.png" )
	bgBottom.width = display.actualContentWidth
	bgBottom.height = display.actualContentHeight
    globals.setBetterAnchor( bgBottom )
    bgBottom.x = display.screenOriginX
    bgBottom.y = bgTop.y + bgTop.contentHeight - 5 --remove noticable padding
    mainGroup:insert( bgBottom )
	
	local coinsBtn = globals.createImageButton( "coins", 
                        0, bgTop.y + bgTop.height - 10,
                        "res/graphics/game/btnCoinsUp.png", "res/graphics/game/btnCoinsDown.png",
                        mainGroup, handleReleaseEvent )
	coinsBtn:setEnabled(false)
	
	local backBtn = globals.createImageButton( "back",
                        display.screenOriginX + 10, bgTop.y + bgTop.height - 10,
                        "res/graphics/menu/btnBackUp.png", "res/graphics/menu/btnBackDown.png",
                        mainGroup, handleReleaseEvent )
    _G.anchor.Center( backBtn )
    backBtn.x = left + ( backBtn.contentWidth * 0.5 ) + 2
    backBtn.y = top + ( backBtn.contentHeight * 0.5 ) + 8
	
		-- only if iphone X
	if onIphoneX then
		backBtn.y = backBtn.y + 20
	end
	
	_G.anchor.Center( coinsBtn )
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
	coinsBtn.y = backBtn.y
	
	local textSettings =
	{
		text = _G.coinVal,
		x = coinsBtn.x - 7,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	mainGroup:insert(coinTxt)
	
	local function showIAPList()
		native.setActivityIndicator(false)
		
		local buttonNum = 6
		if _G.ads == false then
			buttonNum = 5
		end
	
		for i = 1, buttonNum do
			createIAPbuttons(i)
		end
		
		tableView = widget.newTableView({
			left = 0,
			top = bgTop.y + bgTop.height,
			width = display.contentWidth,
			height = display.actualContentHeight - 50,
			hideBackground = true,
			hideScrollBar = true,
			noLines = true,
			onRowRender = onRowRender
		})
		
		mainGroup:insert(tableView)
		
		local isCategory = false
		for i = 1, #shopData do
			if (shopData[i].type == 1 ) then
				tableView:insertRow
				{
					rowHeight = 22,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 2 ) then
				tableView:insertRow
				{
					rowHeight = 39,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 3 ) then
				tableView:insertRow
				{
					rowHeight = 60,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 4 ) then
				tableView:insertRow
				{
					rowHeight = 40,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 0, 0, 0, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			end
		end
	end
	local myRoundedRect, text
	
	local function showConnection() 
		myRoundedRect = display.newRoundedRect( display.contentCenterX, display.contentCenterY, display.actualContentWidth - 80, 100, 15 )
		myRoundedRect.strokeWidth = 3
		myRoundedRect:setStrokeColor( 0.1, 0.4, 0.4 )
		myRoundedRect:scale (0.001,0.001)
		
		popUpGroup:insert(myRoundedRect)
		
		text = display.newText ("Please connect to the Internet to access the shop.", myRoundedRect.x + 5, myRoundedRect.y + 20, myRoundedRect.width - 50, myRoundedRect.height, font, 15)
		text:setFillColor(0,0,0)
		popUpGroup:insert(text)
		text.alpha = 0
		
		transition.to(myRoundedRect, {time = 300, xScale = 1, yScale = 1, transition = easing.inOutBack})
		transition.to(text, {delay = 300, time = 300, alpha = 1})
		
	end
	
	if isOnline then
		if not _G.storeLoaded then
			setupStore()
			_G.storeLoaded = true
		end
	else
		timer.performWithDelay(1500, showConnection, 1 )
	end

	native.setActivityIndicator(true)
	timer.performWithDelay(2000, showIAPList, 1 )
end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif ( phase == "did" ) then
		parent = event.parent
        -- Code here runs when the scene is entirely on screen
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
		composer.removeScene( "_shop" )
		parent:updateCoins()
        -- Code here runs immediately after the scene goes entirely off screen
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene